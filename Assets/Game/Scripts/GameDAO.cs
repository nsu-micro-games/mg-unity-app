using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GameDAO
{
    [Serializable]
    public class MGObjectDAO
    {
        public int id;
        // prefab name (from /Assets/Game/Prefabs)
        public string prefabName;
        public string spriteName;
        public bool hasLocalGraphics;
        public float x;
        public float y;
    }

    [Serializable]
    public class MGTriggerDAO
    {
        public int id;
        /*
         * objectId == 0 : Scene
         */
        public int objectId;
    }

    [Serializable]
    public class MGActionDAO
    {
        public int id;
        /*
         * type == 1 - Move
         * type == 2 - Scale
         * type == 3 - Remove;
         */
        public int type;
        /* type == "Move" : param1 - angle; param2 - distance; param3 - time to move (in sec).
         * type == "Scale" : param1 - scale factor; param2 - time to scale (in sec).
         * type == "Remove" : .
         */
        public double param1;
        public double param2;
        public double param3;
    }

    [Serializable]
    public class MGEventDAO
    {
        public int id;
        public int objectId;
        public int triggerId;
        public int actionId;

        public const int InfRepeatsCount = -1;
        
        public int repeats = InfRepeatsCount;

        //sound filename (from /Assets/Game/Sounds/Effects)
        public string sound;
    }

    // background image filename (from /Assets/Game/Art/Background)
    public string backgroundImage;
    // background music filename (from /Assets/Game/Sounds/Background)
    public string backgroundMusic;
    public MGObjectDAO[] objects;
    public MGTriggerDAO[] triggers;
    public MGActionDAO[] actions;
    public MGEventDAO[] events;
    public double timeout;
    public int[] eventForWinIds;
    public string whatToDo;

}
