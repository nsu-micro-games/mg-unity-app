using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class GameEditorLoader : MonoBehaviour
{
    public GameObject gameList;
    // Start is called before the first frame update
    void Start()
    {
        LoadGamesList();        
    }

    private void LoadGamesList()
    {
        string[] savedGames = Directory.GetFiles(Application.persistentDataPath, "*.json");
        foreach(var gameDir in savedGames)
        {
            string gameName = Path.GetFileName(gameDir);
            gameName = gameName.Substring(0, gameName.Length - 5);

            GameObject newObj = Resources.Load<GameObject>("LoaderGamePanel/GamePanel");
            var _newObj = Instantiate(newObj, new Vector2(0, 0), Quaternion.identity);
            _newObj.name = gameName;
            _newObj.transform.SetParent(gameList.transform);

            _newObj.GetComponentInChildren<UnityEngine.UI.Text>().text = gameName;
        }
    }

    public static void LoadGameToEditor(string fileName)
    {
        string filepath = Application.persistentDataPath + "/" + fileName + ".json";

        if(File.Exists(filepath))
        {
            string fileContent = File.ReadAllText(filepath);
            Editor.gameToLoad = JsonUtility.FromJson<GameDAO>(fileContent);
            Editor.gameName = fileName;
        }
        OpenEditor();
    }

    public static void DeleteGame(string fileName, GameObject objToDelete)
    {
        string filepath = Application.persistentDataPath + "/" + fileName + ".json";

        if(File.Exists(filepath))
        {
            File.Delete(filepath);
        }
        Destroy(objToDelete);
    }

    public static void NewGameToEditor()
    {
        Editor.gameToLoad = null;
        string[] savedGames = Directory.GetFiles(Application.persistentDataPath, "New Game ?.json");
        Editor.gameName = "New Game " + savedGames.Length;
        OpenEditor();   
    }

    public void BackToMenu() {
        SceneManager.LoadSceneAsync("MenuScene", LoadSceneMode.Single);
        
    }

    static void OpenEditor()
    {
        SceneManager.LoadSceneAsync("GameEditorScene", LoadSceneMode.Single);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
