using System.Collections;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PublishWindow : BaseWindow
{
    public TMP_InputField gameTitleField;
    public TMP_InputField descriptionField;
    public Button backButton;
    public Button plusGameTagButton;
    public Button publishButton;
    public static bool IsOpened = false;
    private string _gameName;
    private string _currentGameJson;
    public PublishManager publishManager;

    public void Start()
    {
        publishButton.onClick.AddListener(PublishGame);
        backButton.onClick.AddListener(Back);
        plusGameTagButton.onClick.AddListener(PlusTag);
    }

    private void PlusTag()
    {
        DataHolder.CurrentlyPublishingGameTitle = gameTitleField.text;
        SceneManager.LoadSceneAsync("SearchTag", LoadSceneMode.Single);
    }

    private void Back()
    {
        OnHide();
    }

    public void OnShow(string gameName, string json)
    {
        OnShow();
        _gameName = gameName;
        gameTitleField.text = gameName;
        _currentGameJson = json;
    }

    public override void OnShow()
    {
        base.OnShow();
        IsOpened = true;
    }

    public override void OnHide()
    {
        IsOpened = false;
        base.OnHide();
    }

    private void PublishGame()
    {
        var title = gameTitleField.text;
        var gameJson = _currentGameJson;
        var tagsIds = DataHolder.GetSelectedTagsNames();
        publishManager.Publish(title, descriptionField.text, gameJson, tagsIds, AuthorizationManager.Token, PublishCallback);
    }

    private void PublishCallback(bool success, string result, PublishManager.PublishResponse response)
    {
        if (!success)
        {
            Debug.Log(result);
        }
        else
        {
            Debug.Log("Published successfully, token = " + AuthorizationManager.Token);
        }
    }

    private IEnumerator PublishGameRequest(string title, string gameJson, string[] tagsIds)
    {
        var uwr = UnityWebRequest.Post(DataHolder.BackEndAddress + "/api/games/create", "");
        var requestJson = JsonUtility.ToJson(new DataHolder.GameSending(title, descriptionField.text, gameJson, tagsIds));
        Debug.Log("Publishing: " + requestJson);
        Debug.Log("Token: " + AuthorizationManager.Token);
        uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(requestJson));
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.SetRequestHeader("Authorization", "Bearer " + AuthorizationManager.Token);
        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(uwr.error);
        }
        else
        {
            Debug.Log("Game published successfully");
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }
}