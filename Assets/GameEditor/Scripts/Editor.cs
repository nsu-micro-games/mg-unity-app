using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Linq;
using GameEditor.Resources.ObjectMenu;
using GameEditor.Scripts;
using UnityEngine;
using UnityEngine.UI;
using GamePlayer.Scripts;
using SharedScripts;
using UnityEngine.SceneManagement;

public class Editor : MonoBehaviour
{
    public Button shareButton;
    public Button circleButton;
    public Button saveButton;
    public Button playButton;

    public Slider timeoutSlider;

    public GameDAO gameDAO;
    public static GameDAO gameToLoad;

    public List<GameDAO.MGObjectDAO> objectsList;

    private readonly Dictionary<int, GameObject> createdGameObjects = new Dictionary<int, GameObject>();

    public List<GameDAO.MGTriggerDAO> triggersList;
    public List<GameDAO.MGActionDAO> actionsList;
    public List<GameDAO.MGEventDAO> eventsList;
    public List<int> eventsForWinList;

    public string background;
    public string bgMusic;
    public static string gameName;

    public ObjectMenu objectMenu;
    public GameObject GameField; // TODO use concrete type?
    public GameObject Image; // TODO use concrete type?

    public InputField inputField;

    public string json = "{}";

    private int currentId = 1;

    public GameObject topNavCanvas;
    public PublishWindow publishWindow;

    private GameObject customObjectTemplate;

    private void SetBaseId(int baseId) => currentId = baseId;

    private int NextId()
    {
        var id = currentId;
        currentId += 1;
        return id;
    }

    // Start is called before the first frame update
    private void Start()
    {
        customObjectTemplate = Resources.Load<GameObject>("Prefabs/UserObjectTemplate");

        if (gameToLoad == null)
        {
            gameToLoad = new GameDAO();
        } else
        {
            LoadGame();
        }

        inputField.text = gameName;
        timeoutSlider.value = (float) gameToLoad.timeout;

        shareButton.onClick.AddListener(ShareGame);

        //circleButton.onClick.AddListener(AddObject);

        saveButton.onClick.AddListener(SaveGame);

        playButton.onClick.AddListener(RunGame);

        if (PublishWindow.IsOpened)
        {
            ShareGame();
        }

        Debug.Log("Is publish window opened: " + PublishWindow.IsOpened);
    }

    private void SaveObjects()
    {
        List<GameDAO.MGObjectDAO> objects = new List<GameDAO.MGObjectDAO>();
        foreach (ObjectManager objectManager in GameField.GetComponentsInChildren<ObjectManager>())
        {
            objects.Add(objectManager.objectDao);
        }

        gameDAO.objects = objects.ToArray();
    }

    private void SaveTriggers()
    {
        gameDAO.triggers = triggersList.ToArray();
    }

    private void SaveActions()
    {
        gameDAO.actions = actionsList.ToArray();
    }

    private void SaveEvents()
    {
        gameDAO.events = eventsList.ToArray();
        gameDAO.eventForWinIds = eventsForWinList.ToArray();
    }

    private void SaveTimeout()
    {
        gameDAO.timeout = timeoutSlider.value;
    }

    private void SaveGameName()
    {
        gameName = inputField.text;
    }

    private void SaveGameInFile()
    {
        string saveFile = Application.persistentDataPath + "/" + gameName + ".json";

        File.WriteAllText(saveFile, json);
    }

    private void SaveGame()
    {
        SaveObjects();
        SaveTriggers();
        SaveActions();
        SaveEvents();
        SaveTimeout();
        SaveGameName();
        gameDAO.backgroundImage = background;
        gameDAO.backgroundMusic = bgMusic;
        json = JsonUtility.ToJson(gameDAO);
        gameToLoad = gameDAO;
        SaveGameInFile();
    }

    private void ShareGame()
    {
        Debug.Log("Current game json: " + json);
        Debug.Log("Current game json length: " + json.Length);
        DataHolder.CurrentSearchTagMode = DataHolder.SearchTagMode.ADD_DURING_PUBLISHING;
        SaveGame();
        publishWindow.OnShow(gameName, json);
    }

    public void DeleteObject(int objectId)
    {
        var removed = objectsList.RemoveAll(it => it.id == objectId);
        if (0 == removed)
        {
            Debug.LogWarning($"DAO for object with id {objectId} does not exist");
        }

        if (!createdGameObjects.TryGetValue(objectId, out var obj))
        {
            Debug.LogWarning($"Game object with id {objectId} does not exist");
            return;
        }

        Destroy(obj);
        createdGameObjects.Remove(objectId);
        Debug.Log($"Deleted object {objectId}");

        var triggersToRemove = new HashSet<int>(
            triggersList
                .Where(trigger => trigger.objectId == objectId)
                .Select(trigger => trigger.id)
        );
        triggersList
            .RemoveAll(trigger => triggersToRemove.Contains(trigger.id));

        var eventsToRemove = eventsList
            .Where(@event =>
                @event.objectId == objectId
                || triggersToRemove.Contains(@event.triggerId)
            )
            .ToList();

        foreach (var @event in eventsToRemove)
        {
            eventsList.Remove(@event);
            eventsForWinList.Remove(@event.id);
            triggersList.RemoveAll(trigger => trigger.id == @event.triggerId);
            actionsList.RemoveAll(action => action.id == @event.actionId);
        }

        eventsForWinList.RemoveAll(id => false == eventsList.Any(ev => ev.id == id));

        Debug.Log(
            $"Deleted object with id {objectId}, entities left: {triggersList.Count} triggers, {actionsList.Count} actions, {eventsList.Count} events, {eventsForWinList.Count} events for win");
    }

    private void AddBuiltinObject(int id, float x, float y, string prefabName)
    {
        AddObject(id, x, y, prefabName, null);
    }

    private void AddObjectWithLocalGraphics(int id, float x, float y, string spriteName)
    {
        AddObject(id, x, y, null, spriteName);
    }

    private void AddObject(int id, float x, float y, string prefabName, string spriteName)
    {
        var prefab = null != prefabName
            ? Resources.Load<GameObject>("Prefabs/" + prefabName)
            : customObjectTemplate;

        var objectDao = new GameDAO.MGObjectDAO
        {
            id = id,
            prefabName = prefab.name
        };
        objectsList.Add(objectDao);

        var createdGameObject = Instantiate(prefab, new Vector2(x, y), Quaternion.identity);

        if (null != spriteName)
        {
            Debug.Log($"{nameof(Editor)}.{nameof(AddObject)} Using custom sprite \"{spriteName}\"");
            objectDao.hasLocalGraphics = true;
            objectDao.spriteName = spriteName;

            var sprite = SpriteUtility.LoadFromImage($"{Application.persistentDataPath}/UserSprites/{spriteName}");
            if (null == sprite)
            {
                Debug.Log(
                    $"{nameof(Editor)}.{nameof(AddObject)} \"{spriteName}\" does not exist locally, "
                    + "falling back to the default sprite"
                );
            } else
            {
                createdGameObject.GetComponent<SpriteRenderer>().sprite = sprite;
                createdGameObject.GetComponent<CircleCollider2D>().radius =
                    Math.Min(sprite.bounds.extents.x, sprite.bounds.extents.y);
            }
        }

        createdGameObject.name = objectDao.prefabName + objectDao.id;
        createdGameObject.transform.SetParent(GameField.transform);
        var objectManager = createdGameObject.AddComponent<ObjectManager>();
        objectManager.editor = gameObject.GetComponent<Editor>(); // TODO ... = this?
        objectManager.objectDao = objectDao;
        objectManager.gameField = GameField.GetComponent<GameFieldManager>(); // TODO use concrete type

        if (createdGameObjects.ContainsKey(objectDao.id))
        {
            Debug.LogError($"{nameof(Editor)}.{nameof(AddObject)} Object with id={objectDao.id} already exists");
        }

        Debug.Log($"{nameof(Editor)}.{nameof(AddObject)} Object id={objectDao.id}");
        createdGameObjects[objectDao.id] = createdGameObject;

        Debug.Log($"{nameof(Editor)}.{nameof(AddObject)} Created object with id={objectDao.id}");
    }

    public void NewBuiltinObject(string prefabName)
    {
        AddBuiltinObject(NextId(), 0.0f, 0.0f, prefabName);
    }

    public void NewObjectWithLocalGraphics(string spriteName)
    {
        AddObjectWithLocalGraphics(NextId(), 0.0f, 0.0f, spriteName);
    }

    private void AddTrigger(int id, int objId)
    {
        var newTrigger = new GameDAO.MGTriggerDAO
        {
            objectId = objId,
            id = id
        };
        triggersList.Add(newTrigger);

        // ObjectMenu.GetComponent<ObjectMenuScript>().SetSelectedTrigger(newTrigger.id);
    }

    public int AddTrigger(int objId)
    {
        var triggerId = NextId();
        AddTrigger(triggerId, objId);
        return triggerId;
    }

    private void AddAction(int id, int actionType, List<double> actionParams)
    {
        var newAction = new GameDAO.MGActionDAO
        {
            id = id,
            type = actionType,
            param1 = actionParams[0],
            param2 = actionParams[1],
            param3 = actionParams.Count > 2 ? actionParams[2] : 0f
        };
        actionsList.Add(newAction);

        // ObjectMenu.GetComponent<ObjectMenuScript>().SetSelectedActionId(newAction.id);
    }

    public int AddAction(int actionType, List<double> actionParams)
    {
        var actionId = NextId();
        AddAction(actionId, actionType, actionParams);
        return actionId;
    }

    public void AddAction(int actionType)
    {
        List<double> props = new List<double> {0.0, 0.0, 0.0};
        AddAction(actionType, props);
    }

    public void AddEvent(GameDAO.MGEventDAO newEvent, bool isForWin)
    {
        eventsList.Add(newEvent);

        if (isForWin)
        {
            eventsForWinList.Add(newEvent.id);
        }
    }

    public void AddEvent(
        int triggerId,
        int targetObjectId,
        int actionId,
        string soundName,
        bool requiredForWinning,
        int repeatsCount
    )
    {
        var newEvent = new GameDAO.MGEventDAO
        {
            id = NextId(),
            triggerId = triggerId,
            actionId = actionId,
            sound = soundName,
            objectId = targetObjectId,
            repeats = repeatsCount
        };

        AddEvent(newEvent, requiredForWinning);
    }

    public void ShowObjectMenu(int objectId)
    {
        gameObject.SetActive(false);
        GameField.SetActive(false);

        objectMenu.Show(objectId);
    }

    public void SetBackground(string name)
    {
        background = name;
        Image.GetComponent<Image>().sprite = Resources.Load<Sprite>("Art/Background/" + name);
    }

    public void BackToLoad()
    {
        SaveGame();
        SceneManager.LoadSceneAsync("GameEditorLoad", LoadSceneMode.Single);
    }

    public void RunGame()
    {
        SaveGame();
        var gameData = new GameData
        {
            authorId = GameData.LocalAuthorID,
            gameJson = JsonUtility.FromJson<GameDAO>(json)
        };

        GameManager.Init();
        GameManager.gamesQueue.Enqueue(gameData);
        PlayerManager.StartPlayer();
    }

    private void LoadGame()
    {
        var maxId = gameToLoad.objects.Select(it => it.id)
                .Concat(gameToLoad.triggers.Select(it => it.id))
                .Concat(gameToLoad.actions.Select(it => it.id))
                .Concat(gameToLoad.events.Select(it => it.id))
                .Select(it => (int?) it)
                .Max()
            ?? 1;
        Debug.Log($"{nameof(Editor)}.{nameof(Show)} {nameof(maxId)}={maxId}");
        SetBaseId(maxId + 1);

        foreach (var objectDao in gameToLoad.objects)
        {
            if (objectDao.hasLocalGraphics)
            {
                AddObjectWithLocalGraphics(objectDao.id, objectDao.x, objectDao.y, objectDao.spriteName);
            } else
            {
                AddBuiltinObject(objectDao.id, objectDao.x, objectDao.y, objectDao.prefabName);
            }

            Debug.Log($"{nameof(Editor)}.{nameof(Show)} Loaded object with id={objectDao.id}");
        }

        foreach (var actionDao in gameToLoad.actions)
        {
            List<double> actionParams = new List<double> {actionDao.param1, actionDao.param2, actionDao.param3};
            AddAction(actionDao.id, actionDao.type, actionParams);
            Debug.Log($"{nameof(Editor)}.{nameof(Show)} Loaded action with id={actionDao.id}");
        }

        foreach (var triggerDao in gameToLoad.triggers)
        {
            AddTrigger(triggerDao.id, triggerDao.objectId);
            Debug.Log($"{nameof(Editor)}.{nameof(Show)} Loaded trigger with id={triggerDao.id}");
        }

        foreach (var eventDao in gameToLoad.events)
        {
            AddEvent(eventDao, Array.Exists(gameToLoad.eventForWinIds, id => id == eventDao.id));
            Debug.Log($"{nameof(Editor)}.{nameof(Show)} Loaded event with id={eventDao.id}");
        }

        eventsForWinList = new List<int>(gameToLoad.eventForWinIds);
        bgMusic = gameToLoad.backgroundMusic;
        SetBackground(gameToLoad.backgroundImage);
    }

    public void Show()
    {
        Debug.Log($"{nameof(Editor)}.{nameof(Show)}");
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        Debug.Log($"{nameof(Editor)}.{nameof(Hide)}");
        gameObject.SetActive(false);
    }
}