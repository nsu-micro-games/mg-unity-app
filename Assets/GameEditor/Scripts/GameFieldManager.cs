using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameEditor.Scripts
{
    public class GameFieldManager : MonoBehaviour
    {
        // TODO rename to objectSelectionMode or something similar
        private bool triggerChoice;
        public bool SelectionModeActive => triggerChoice;

        private bool moveChoice;
        public bool MoveModeActive => moveChoice;

        public GameObject topPanel;
        public Text topText;

        public Button setBackground;

        public GameObject bottomPanel;
        public Button cancelButton;

        public Button backToObjectMenuButton;
        public MovePropsScript movePropsScript;

        private LineRenderer lineRenderer;

        private Action<int> objectSelected;
        // TODO cancel option should be implemented here 
        private Action selectionCancelled;

        private Action<double, double> moveVectorSelected;
        private Action moveSelectionCancelled;

        // Start is called before the first frame update
        void Start()
        {
            lineRenderer = new GameObject("Line").AddComponent<LineRenderer>();
            lineRenderer.startColor = Color.white;
            lineRenderer.endColor = Color.white;
            lineRenderer.startWidth = 1f;
            lineRenderer.endWidth = 1f;
            lineRenderer.positionCount = 2;
            lineRenderer.useWorldSpace = true;
            lineRenderer.transform.SetParent(transform);

            cancelButton.onClick.AddListener(OnCancelPressed);
        }

        private void OnCancelPressed()
        {
            Debug.Log($"{nameof(GameFieldManager)}.{nameof(OnCancelPressed)},"
                + $" {nameof(triggerChoice)}={triggerChoice}");

            if (false == triggerChoice)
            {
                return;
            }

            selectionCancelled?.Invoke();
            ExitObjectSelectionMode();
        }

        public void EnterObjectSelectionMode(Action<int> onObjectSelected, Action onCanceled)
        {
            Debug.Log($"{nameof(GameFieldManager)}.{nameof(EnterObjectSelectionMode)}");

            if (triggerChoice)
            {
                Debug.LogError($"`{nameof(triggerChoice)}` is already enabled");
            }

            topPanel.SetActive(true);
            topText.text = "Select an object"; // TODO use named constant

            bottomPanel.SetActive(true);

            triggerChoice = true;
            objectSelected = onObjectSelected;
            selectionCancelled = onCanceled;
        }

        public void ObjectSelected(int id)
        {
            var callback = objectSelected;
            ExitObjectSelectionMode();
            callback?.Invoke(id);
        }

        private void ExitObjectSelectionMode()
        {
            Debug.Log($"{nameof(GameFieldManager)}.{nameof(ExitObjectSelectionMode)}");

            if (false == triggerChoice)
            {
                Debug.LogError($"`{nameof(triggerChoice)}` was not enabled");
            }

            topPanel.SetActive(false);
            bottomPanel.SetActive(false);

            triggerChoice = false;
            objectSelected = null;
            selectionCancelled = null;
        }

        public void EnterMoveVectorSelectionMode(
            int originId,
            Action<double, double> onMoveVectorSelected,
            Action onCanceled
        )
        {
            Debug.Log($"{nameof(GameFieldManager)}.{nameof(EnterMoveVectorSelectionMode)}");

            movePropsScript.objId = originId;

            if (moveChoice)
            {
                Debug.LogError($"`{nameof(moveChoice)}` was not enabled");
            }

            topPanel.SetActive(true);
            topText.text = "Select destination"; // TODO use named constant

            bottomPanel.SetActive(true);

            moveChoice = true;
            moveVectorSelected = onMoveVectorSelected;
            moveSelectionCancelled = onCanceled;
        }

        private void MoveDestinationSelected(double angle, double distance)
        {
            var callback = moveVectorSelected;
            ExitMoveVectorSelectionMode();
            callback?.Invoke(angle, distance);
        }

        public void ExitMoveVectorSelectionMode()
        {
            Debug.Log($"{nameof(GameFieldManager)}.{nameof(ExitMoveVectorSelectionMode)}");

            if (false == moveChoice)
            {
                Debug.LogError($"`{nameof(moveChoice)}` is already enabled");
            }

            topPanel.SetActive(false);
            bottomPanel.SetActive(false);

            moveChoice = false;
            moveVectorSelected = null;
            moveSelectionCancelled = null;
        }

        public void SetTriggerChoice(bool isTriggerChoice)
        {
            triggerChoice = isTriggerChoice;
        }

        public void SetMoveChoice(bool isMoveChoice)
        {
            moveChoice = isMoveChoice;
        }

        private void OnMouseDrag()
        {
            lineRenderer.GetComponent<LineRenderer>().enabled = true;
            Vector2 curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            Vector2 movePoint = new Vector2(Camera.main.ScreenToWorldPoint(curScreenPoint).x, Camera.main.ScreenToWorldPoint(curScreenPoint).y);
            Vector2 objPoint = getObjVector(movePropsScript.objId);
            movePropsScript.distance = Vector2.Distance(objPoint, movePoint);
            movePropsScript.angle = Vector2.Angle(Vector2.right, movePoint - objPoint);
            drawVector(objPoint, movePoint);
            if (objPoint.y > movePoint.y)
            {
                movePropsScript.angle = 360f - movePropsScript.angle;
            }
        }

        private void OnMouseUp()
        {
            if (false == moveChoice)
            {
                return;
            }
            
            MoveDestinationSelected(movePropsScript.angle, movePropsScript.distance);
        }

        private void drawVector(Vector2 from, Vector2 to)
        {
            lineRenderer.SetPosition(0, from);
            lineRenderer.SetPosition(1, to);
        }

        private Vector2 getObjVector(int objId)
        {
            ObjectManager[] objects = transform.GetComponentsInChildren<ObjectManager>();
            foreach (ObjectManager ob in objects)
            {
                if (ob.objectDao.id == objId)
                {
                    return new Vector2(ob.objectDao.x, ob.objectDao.y);
                }
            }
            return Vector2.positiveInfinity;
        }

        // Update is called once per frame
        void Update()
        {
            gameObject.GetComponent<BoxCollider2D>().enabled = moveChoice;
            if (!moveChoice)
            {
                lineRenderer.GetComponent<LineRenderer>().enabled = false;
            }

            setBackground.enabled = false == (MoveModeActive || SelectionModeActive);
        }

        public void Show()
        {
            Debug.Log($"{nameof(GameFieldManager)}.{nameof(Show)}");
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            Debug.Log($"{nameof(GameFieldManager)}.{nameof(Hide)}");
            gameObject.SetActive(false);
        }
    }
}