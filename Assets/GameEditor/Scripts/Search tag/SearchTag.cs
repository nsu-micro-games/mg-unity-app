using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SearchTag : MonoBehaviour
{
    public Button backButton;

    public bool Initialized {get; set;} = false;

    public void Start() {
        if (!DataHolder.AvailableTagsPresent()) {
            InitializeAvailableTags();
        }

        Initialized = true;
        backButton.onClick.AddListener(Back);
    }

    private void InitializeAvailableTags() {
        List<DataHolder.Tag> tags = new List<DataHolder.Tag>();
        for (int i = 0; i < 10; i++) {
            tags.Add(new DataHolder.Tag(i, "Tag #" + i));
        }
        DataHolder.AvailableTags = tags;
    }

    public void Back() {
        DataHolder.SearchTagMode mode = DataHolder.CurrentSearchTagMode;
        switch(mode) {
            case DataHolder.SearchTagMode.ADD_DURING_PUBLISHING:
                SceneManager.LoadSceneAsync("GameEditorScene", LoadSceneMode.Single);
                break;
            case DataHolder.SearchTagMode.ADD_FILTER:
                SceneManager.LoadScene("Filter", LoadSceneMode.Single);
                break;
        }
    }
}