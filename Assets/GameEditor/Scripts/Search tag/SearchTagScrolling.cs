using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SearchTagScrolling : MonoBehaviour
{
    public GameObject tagButtonPrefab;
    public int buttonsOffset = 20;

    private GameObject[] buttons;
    private SearchTag stComponent;

    public void Start()
    {
        stComponent = (SearchTag)GameObject.Find("Controller").GetComponent("SearchTag");
        StartCoroutine("Wait");
    }

    private IEnumerator Wait()
    {
        while (stComponent.Initialized != true)
        {
            yield return null;
        }

        Initianization();
    }

    private void Initianization()
    {
        List<DataHolder.Tag> tags = DataHolder.AvailableTags;
        buttons = new GameObject[tags.Count];

        for (int i = 0; i < tags.Count; i++)
        {
            buttons[i] = Instantiate(tagButtonPrefab, transform, false);
            SetValuesForButton(i, tags[i]);
            if (i == 0) continue;
            buttons[i].transform.localPosition = new Vector2(
                buttons[i].transform.localPosition.x,
                buttons[i - 1].transform.localPosition.y - tagButtonPrefab.GetComponent<RectTransform>().sizeDelta.y -
                buttonsOffset);
        }
    }

    private void SetValuesForButton(int buttonIndex, DataHolder.Tag tag)
    {
        SearchTagButton component = (SearchTagButton)buttons[buttonIndex].GetComponent("SearchTagButton");
        component.TagId = tag.Id;
        component.TagName = tag.Name;
        buttons[buttonIndex].GetComponentInChildren<TextMeshProUGUI>().text = component.TagName;
    }
}