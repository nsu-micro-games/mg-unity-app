using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SearchTagButton : MonoBehaviour {
    
    public long TagId {get; set;}
    public string TagName {get; set;}

    public void OnClick() {
        DataHolder.SelectTag(new DataHolder.Tag(TagId, TagName));

        DataHolder.SearchTagMode mode = DataHolder.CurrentSearchTagMode;
        switch(mode) {
            case DataHolder.SearchTagMode.ADD_DURING_PUBLISHING:
                SceneManager.LoadSceneAsync("GameEditorScene", LoadSceneMode.Single);
                break;
            case DataHolder.SearchTagMode.ADD_FILTER:
                SceneManager.LoadScene("Filter", LoadSceneMode.Single);
                break;
        }
    }
}