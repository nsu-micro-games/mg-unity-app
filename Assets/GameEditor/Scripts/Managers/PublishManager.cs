using System;
using UnityEngine;

public class PublishManager : MonoBehaviour
{
    public RestClient restClient;

    public void Publish(string title, string description, string gameJson, string[] tagsIds, string token,
        Action<bool, string, PublishResponse> callback)
    {
        restClient.PostRequest<PublishResponse>(DataHolder.BackEndAddress + "/api/games/create",
            new DataHolder.GameSending(title, description, gameJson, tagsIds),
            (b, s, r) => PublishCallback(b, s, r, callback), token);
    }

    private void PublishCallback(bool success, string result, PublishResponse response,
        Action<bool, string, PublishResponse> callback)
    {
        callback(success, result, response);
    }

    public class PublishResponse
    {
        public int Id;
    }
}