using GameEditor.Scripts;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class ObjectManager : MonoBehaviour
{
    [FormerlySerializedAs("MGObject")] 
    public GameDAO.MGObjectDAO objectDao;
    
    public Editor editor;
    public GameFieldManager gameField;

    private Vector2 screenPoint;
    private Vector2 offset;

    private bool isTriggerChoice;
    private bool isMoveChoice;

    // Start is called before the first frame update
    void Start()
    {

    }

    void OnMouseDown()
    {
        if((isTriggerChoice || isMoveChoice) == false)
        {
            screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

            offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
        }
        else if (gameField.SelectionModeActive)
        {
            gameField.ObjectSelected(objectDao.id);
        }
    }

    private void OnMouseDrag()
    {
        if((isTriggerChoice || isMoveChoice) == false)
        {
            Vector2 curScreenPoint = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            Vector2 curPosition = new Vector2(Camera.main.ScreenToWorldPoint(curScreenPoint).x, Camera.main.ScreenToWorldPoint(curScreenPoint).y) + offset;
            transform.position = curPosition;
        }
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePosition();
        isTriggerChoice = transform.GetComponentInParent<GameFieldManager>().SelectionModeActive;
        isMoveChoice = transform.GetComponentInParent<GameFieldManager>().MoveModeActive;
        if(isMoveChoice || isTriggerChoice) {
            editor.Image.GetComponent<Button>().enabled = false;
        } else {
            editor.Image.GetComponent<Button>().enabled = true;
        }
    }

    private void OnMouseOver()
    {
        editor.Image.GetComponent<Button>().enabled = false;
        if(Input.GetMouseButtonDown(1))
        {
            editor.ShowObjectMenu(objectDao.id);
        }   
    }

    private void OnMouseExit() {
        editor.Image.GetComponent<Button>().enabled = true;
    }

    void AddTrigger()
    {
        editor.AddTrigger(objectDao.id);
    }

    void UpdatePosition()
    {
        objectDao.x = transform.position.x;
        objectDao.y = transform.position.y;
    }
}
