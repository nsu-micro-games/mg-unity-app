using UnityEngine;
using UnityEngine.UI;
namespace GameEditor.Resources.SelectSoundMenu
{
    public class SoundListItem : MonoBehaviour
    {
        public SoundMenu soundMenu;
        public Text text;
            
        private AudioClip clip;

        public Button listenButton;
        public Button selectButton;

        private void Start()
        {
            listenButton.onClick.AddListener(Play);
            selectButton.onClick.AddListener(Select);
        }

        public void SetAudioClip(AudioClip audioClip)
        {
            clip = audioClip;
            text.text = clip.name;
        }

        private void Play() => soundMenu.SetAudioClip(clip);

        private void Select() => soundMenu.SetSelectedAudio(clip.name);
    }
}