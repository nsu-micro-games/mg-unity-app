using UnityEngine;
using UnityEngine.UI;

namespace GameEditor.Resources.SelectSoundMenu
{
    public class SoundMenu : MonoBehaviour
    {
        public AudioSource audioSource;
        public Transform soundsList;
        public Button backButton;

        public EventsMenu.EventsMenu eventsMenu;

        private void Start()
        {
            backButton.onClick.AddListener(() =>
            {
                Hide();
                eventsMenu.Show();
            });

            var soundSources = UnityEngine.Resources.LoadAll<AudioClip>("Sounds/Effects");

            foreach (var sound in soundSources)
            {
                var soundItem = Instantiate(
                    UnityEngine.Resources.Load<GameObject>("SelectSoundMenu/SoundListItem"),
                    parent: soundsList
                );
                var soundListItem = soundItem.GetComponent<SoundListItem>();
                soundListItem.SetAudioClip(sound);
                soundListItem.soundMenu = this;
            }
        }

        public void SetAudioClip(AudioClip clip)
        {
            // audioSource.Stop();
            // audioSource.clip = clip;
            // audioSource.Play();
            
            audioSource.PlayOneShot(clip);
        }

        public void SetSelectedAudio(string audioName)
        {
            Debug.Log($"{nameof(SoundMenu)}.{nameof(SetSelectedAudio)}: audioName={audioName}");
            
            eventsMenu.SetSelectedSoundName(audioName);
            backButton.onClick.Invoke();
        }

        public void Show()
        {
            Debug.Log($"{nameof(SoundMenu)}.{nameof(Show)}");
            gameObject.SetActive(true);
        }

        private void Hide()
        {
            Debug.Log($"{nameof(SoundMenu)}.{nameof(Hide)}");
            gameObject.SetActive(false);
        }
    }
}