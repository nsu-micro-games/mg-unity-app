using UnityEngine;
using UnityEngine.UI;
namespace GameEditor.Resources.SpriteGallery
{

    public class GalleryItemScript : MonoBehaviour
    {
        public bool hasLocalGraphics;

        public Button button;
        public Image image;
        public SpriteGalleryScript spriteGallery;

        private void Start()
        {
            button.onClick.AddListener(Selected);
        }

        public void SetSprite(Sprite sprite)
        {
            image.sprite = sprite;
            image.SetNativeSize();
        }
        private void Selected()
        {
            if (hasLocalGraphics)
            {
                spriteGallery.LocalSpriteSelected(gameObject.name);
            } else
            {
                spriteGallery.BuiltinSpriteSelected(gameObject.name);
            }
        }
    }
}