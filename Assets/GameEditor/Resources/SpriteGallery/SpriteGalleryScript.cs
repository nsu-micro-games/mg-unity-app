using System.IO;
using System.Linq;
using SharedScripts;
using SimpleFileBrowser;
using UnityEngine;
using UnityEngine.UI;

namespace GameEditor.Resources.SpriteGallery
{
    public class SpriteGalleryScript : MonoBehaviour
    {
        public Editor editor;
        public Button backButton;

        public GridLayoutGroup gridLayoutGroup;
        public ScrollRect scrollRect;

        public Button addSpriteButton;

        private static readonly string[] BuiltinSpriteNames =
        {
            "Cow",
            "Division_sign",
            "Sheep",
            "Button",
            "Circle",
            "Equal_sign",
            "Multiplication_sign",
            "Plus_sign",
            "Question_mark",
            "Spaceship_tall",
            "Spaceship_wide",
            "Substraction_sign",
            "Ufo",
            "Venera",
            "Venera_with_arms"
        };

        private void Start()
        {
            FileBrowser.SetFilters(
                true,
                new FileBrowser.Filter("Images", ".jpg", ".jpeg", ".png")
            );
            FileBrowser.SetDefaultFilter(".png");

            LoadSprites();
            addSpriteButton.onClick.AddListener(OpenFileDialog);
        }

        private void LoadSprites()
        {
            foreach (var itemName in BuiltinSpriteNames)
            {
                var sprite = UnityEngine.Resources.Load<SpriteRenderer>("Prefabs/" + itemName).sprite;
                AddMenuItem(itemName, sprite);
            }

            var userSpritesDir = new DirectoryInfo($"{Application.persistentDataPath}/UserSprites");
            if (false == userSpritesDir.Exists)
            {
                Debug.Log(
                    $"{nameof(SpriteGalleryScript)}.{nameof(Start)} "
                    + $"Directory for user sprites does not exist, nothing to load"
                );
                return;
            }
            foreach (var file in userSpritesDir.GetFiles())
            {
                var sprite = SpriteUtility.LoadFromImage(file.FullName);
                AddMenuItem(file.Name, sprite);
            }
        }

        private void AddMenuItem(string itemName, Sprite sprite)
        {
            var galleryItem = Instantiate(
                UnityEngine.Resources.Load<GalleryItemScript>("SpriteGallery/SpriteGalleryItem"),
                gridLayoutGroup.transform,
                false
            );

            galleryItem.name = itemName;
            galleryItem.spriteGallery = this;
            galleryItem.SetSprite(sprite);

            if (false == BuiltinSpriteNames.Contains(itemName))
            {
                galleryItem.hasLocalGraphics = true;
            }

            Debug.Log($"{nameof(SpriteGalleryScript)}.{nameof(Start)} Added {itemName}");
        }

        private void OpenFileDialog()
        {
            FileBrowser.ShowLoadDialog(
                OnFileSelected,
                () => Debug.Log($"{nameof(SpriteGalleryScript)}.<onCancel> File selection cancelled"),
                FileBrowser.PickMode.Files,
                title: "Select Image"
            );
        }

        private void OnFileSelected(string[] paths)
        {
            var imagePath = paths[0];
            var imageFileName = Path.GetFileName(imagePath);
            var userSpritesDirectory = $"{Application.persistentDataPath}"
                + $"{Path.DirectorySeparatorChar}UserSprites";
            if (false == File.Exists(userSpritesDirectory))
            {
                Directory.CreateDirectory(userSpritesDirectory);
            }
            var savedPath =  $"{userSpritesDirectory}{Path.DirectorySeparatorChar}{imageFileName}";

            if (File.Exists(savedPath))
            {
                Debug.Log(
                    $"{nameof(GalleryItemScript)}.{nameof(OnFileSelected)} "
                    + $"{savedPath} already exists"
                );
                return;
            }

            var sprite = SpriteUtility.LoadFromImage(imagePath);
            if (null == sprite)
            {
                Debug.LogError(
                    $"{nameof(GalleryItemScript)}.{nameof(OnFileSelected)} Failed to load \"{imagePath}\""
                );
                return;
            }
            AddMenuItem(imageFileName, sprite);
            Canvas.ForceUpdateCanvases();
            scrollRect.normalizedPosition = Vector2.zero;
            File.Copy(imagePath, savedPath);

            Debug.Log(
                $"{nameof(GalleryItemScript)}.{nameof(OnFileSelected)} "
                + $"Saved \"{imageFileName}\" to {savedPath}"
            );
        }

        public void BuiltinSpriteSelected(string prefabName)
        {
            backButton.onClick.Invoke();
            editor.NewBuiltinObject(prefabName);
        }

        public void LocalSpriteSelected(string spriteName)
        {
            backButton.onClick.Invoke();
            editor.NewObjectWithLocalGraphics(spriteName);
        }
    }
}