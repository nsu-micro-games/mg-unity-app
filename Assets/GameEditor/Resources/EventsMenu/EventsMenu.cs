using System;
using System.Collections.Generic;
using GameEditor.Resources.SelectSoundMenu;
using GameEditor.Scripts;
using UnityEngine;
using UnityEngine.UI;
namespace GameEditor.Resources.EventsMenu
{
    public class EventsMenu : MonoBehaviour
    {
        public NewTriggerMenu.NewTriggerMenu newTriggerMenu;
        public Button newTriggerButton;

        public Text selectedTriggerText;

        public NewActionMenu.NewActionMenu newActionMenu;
        public Button newActionButton;

        public Text selectedActionTargetText;

        public SoundMenu selectSoundMenu;
        public Button selectSoundButton;

        public Text selectedSoundNameText;

        public Toggle requiredForWinningToggle;

        public Text repeatsCountText;
        public Button increaseRepeatsCountButton;
        public Button decreaseRepeatsCountButton;

        public Button backButton;
        public Button saveButton;

        public Editor editor;
        public GameFieldManager gameField;

        private const int DefaultTriggerObjectId = -1;
        private int triggerObjectId = DefaultTriggerObjectId;

        private const int DefaultTargetObjectId = -1;
        private int actionTargetObjectId = DefaultTargetObjectId;

        private const int DefaultActionType = -1;
        private int actionType = DefaultActionType;

        private static readonly List<double> DefaultActionParameters = null;
        private List<double> actionParameters = DefaultActionParameters;

        private const string DefaultAudioName = "";
        private string audioName = DefaultAudioName;

        private const bool DefaultRequiredForWinning = true;
        private bool savedRequiredForWinning = DefaultRequiredForWinning;
        private bool requiredForWinning = DefaultRequiredForWinning;

        private const int DefaultRepeatsCount = 1;
        private const string DefaultRepeatsCountString = "Once";

        private const int MinRepeatsCount = 1;
        private const int MaxRepeatsCount = 99;
        private const int InfRepeatsCount = GameDAO.MGEventDAO.InfRepeatsCount;

        private int repeatsCount = DefaultRepeatsCount;

        private void Start()
        {
            CheckCanSave();

            backButton.onClick.AddListener(() =>
            {
                Debug.Log($"{nameof(EventsMenu)}: Back");
                Hide(showEditor: true);
            });

            newTriggerButton.onClick.AddListener(() =>
            {
                Hide();
                newTriggerMenu.Show();
            });

            newActionButton.onClick.AddListener(() =>
            {
                Hide();
                newActionMenu.Show();
            });

            selectSoundButton.onClick.AddListener(() =>
            {
                Hide();
                selectSoundMenu.Show();
            });

            saveButton.onClick.AddListener(AddEvent);
            saveButton.onClick.AddListener(() => Hide(showEditor: true));

            requiredForWinningToggle.isOn = requiredForWinning;
            requiredForWinningToggle.onValueChanged
                .AddListener(value => requiredForWinning = value);

            increaseRepeatsCountButton.onClick.AddListener(IncreaseRepeatsCount);
            decreaseRepeatsCountButton.onClick.AddListener(DecreaseRepeatsCount);
            UpdateRepeatsCountUI();
        }

        private void ResetTriggerObject()
        {
            triggerObjectId = -1;
            selectedTriggerText.text = "None";
        }

        public EventsMenu SetSelectedTriggerObject(int id)
        {
            Debug.Log($"{nameof(EventsMenu)}.{nameof(SetSelectedTriggerObject)}: id={id}");

            triggerObjectId = id;
            selectedTriggerText.text =
                id == NewTriggerMenu.NewTriggerMenu.ScreenObjectID
                    ? "Tap on screen"
                    : $"Tap on object #{id}";

            CheckCanSave();

            return this;
        }

        private void ResetAction()
        {
            actionTargetObjectId = -1;
            actionType = -1;
            selectedActionTargetText.text = "None";
        }

        public EventsMenu SetRemoveParameters(int id)
        {
            Debug.Log($"{nameof(EventsMenu)}.{nameof(SetRemoveParameters)}: id={id}");

            actionTargetObjectId = id;
            actionType = 3;
            actionParameters = new List<double> {0.0, 0.0, 0.0};
            selectedActionTargetText.text =
                $"Remove object #{id}";

            CheckCanSave();

            return this;
        }

        public EventsMenu SetScaleParameters(int id, double scale, double duration)
        {
            Debug.Log($"{nameof(EventsMenu)}.{nameof(SetScaleParameters)}: id={id}, scale={scale}, duration={duration}");

            actionTargetObjectId = id;
            actionType = 2;
            actionParameters = new List<double> {scale, duration, 0.0};
            selectedActionTargetText.text =
                $"Scale #{id} to x{scale:f1} over {duration:f1}s";

            CheckCanSave();

            return this;
        }

        public EventsMenu SetMoveParameters(int id, double angle, double distance, double duration)
        {
            Debug.Log($"{nameof(EventsMenu)}.{nameof(SetMoveParameters)}: id={id}, angle={angle}, distance={distance}, duration={duration}");

            actionTargetObjectId = id;
            actionType = 1;
            actionParameters = new List<double> {angle, distance, duration};
            selectedActionTargetText.text =
                $"Move #{id} {distance:f1} units at {angle:f0}° in {duration:f1}s";

            CheckCanSave();

            return this;
        }

        private void ResetSound()
        {
            audioName = "";
            selectedSoundNameText.text = "None";
        }

        public EventsMenu SetSelectedSoundName(string soundName)
        {
            Debug.Log($"{nameof(EventsMenu)}.{nameof(SetSelectedSoundName)}: soundName={soundName}");

            audioName = soundName;
            selectedSoundNameText.text = soundName;

            CheckCanSave();

            return this;
        }

        private void ResetRequiredForWinningToggle()
        {
            requiredForWinningToggle.isOn = DefaultRequiredForWinning;
        }

        private void ResetRepeatsCount()
        {
            repeatsCount = DefaultRepeatsCount;
            UpdateRepeatsCountUI();
        }

        private void UpdateRepeatsCountUI()
        {
            repeatsCountText.text = repeatsCount switch
            {
                InfRepeatsCount => "∞",
                DefaultRepeatsCount => DefaultRepeatsCountString,
                _ => $"{repeatsCount} times"
            };

            if (repeatsCount == InfRepeatsCount)
            {
                savedRequiredForWinning = requiredForWinning;
                requiredForWinningToggle.isOn = false;
                requiredForWinningToggle.enabled = false;
            } else if (false == requiredForWinningToggle.enabled)
            {
                requiredForWinningToggle.enabled = true;
                requiredForWinningToggle.isOn = savedRequiredForWinning;

            }

        }

        private void IncreaseRepeatsCount()
        {
            repeatsCount = InfRepeatsCount == repeatsCount
                ? MinRepeatsCount
                : Math.Min(repeatsCount + 1, MaxRepeatsCount);

            UpdateRepeatsCountUI();
        }

        private void DecreaseRepeatsCount()
        {
            repeatsCount = repeatsCount switch
            {
                InfRepeatsCount => InfRepeatsCount,
                MinRepeatsCount => InfRepeatsCount,
                _ => Math.Max(repeatsCount - 1, MinRepeatsCount)
            };

            UpdateRepeatsCountUI();
        }

        public void Show()
        {
            Debug.Log($"{nameof(EventsMenu)}.{nameof(Show)}");

            gameObject.SetActive(true);
            HideEditor();
        }

        public void Hide(bool showEditor = false)
        {
            Debug.Log($"{nameof(EventsMenu)}.{nameof(Hide)}");

            gameObject.SetActive(false);

            if (showEditor)
            {
                ShowEditor();
            }
        }

        private void ShowEditor()
        {
            Debug.Log($"{nameof(EventsMenu)}.{nameof(ShowEditor)}");

            editor.Show();
            gameField.Show();
        }

        private void HideEditor()
        {
            Debug.Log($"{nameof(EventsMenu)}.{nameof(HideEditor)}");

            editor.Hide();
            gameField.Hide();
        }

        private bool CanAddEvent()
        {
            return
                triggerObjectId != DefaultTriggerObjectId
                && actionTargetObjectId != DefaultTargetObjectId
                && actionType != DefaultActionType
                && actionParameters != DefaultActionParameters;
        }

        private void CheckCanSave()
        {
            saveButton.enabled = CanAddEvent();
        }

        private void ResetSelection()
        {
            ResetTriggerObject();
            ResetAction();
            ResetSound();
            ResetRequiredForWinningToggle();
            ResetRepeatsCount();

            CheckCanSave();
        }

        private void AddEvent()
        {
            editor.AddEvent(
                editor.AddTrigger(triggerObjectId),
                actionTargetObjectId,
                editor.AddAction(actionType, actionParameters),
                audioName,
                requiredForWinning,
                repeatsCount
            );

            ResetSelection();
        }
    }
}