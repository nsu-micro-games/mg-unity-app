using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BGGalleryScript : MonoBehaviour
{
    public Editor editor;
    public Button backButton;
    // Start is called before the first frame update
    void Start()
    {

    }

    public void setBackground(string spriteName)
    {
        backButton.onClick.Invoke();
        editor.SetBackground(spriteName);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
