using GameEditor.Scripts;
using UnityEngine;
using UnityEngine.UI;
namespace GameEditor.Resources.NewTriggerMenu
{
    public class NewTriggerMenu : MonoBehaviour
    {
        public GameFieldManager gameFieldManager;
        public Button tapObjectTriggerTypeButton;
        public Button tapScreenTriggerTypeButton;

        public Button backButton;

        public EventsMenu.EventsMenu eventsMenu;

        public const int ScreenObjectID = 0;

        public void Start()
        {
            backButton.onClick.AddListener(() =>
            {
                Debug.Log($"{nameof(NewTriggerMenu)}: Back");
                Hide();
                eventsMenu.Show();
            });

            tapObjectTriggerTypeButton.onClick.AddListener(() =>
            {
                Hide();
                gameFieldManager.Show();
                gameFieldManager.EnterObjectSelectionMode(OnObjectSelected, OnSelectionCancelled);
            });

            tapScreenTriggerTypeButton.onClick.AddListener(() =>
            {
                Hide();
                eventsMenu
                    .SetSelectedTriggerObject(id: ScreenObjectID)
                    .Show();
            });
        }

        // Callback to be passed to GameFieldManager
        private void OnObjectSelected(int id)
        {
            gameFieldManager.Hide();
            
            eventsMenu
                .SetSelectedTriggerObject(id)
                .Show();
        }

        private void OnSelectionCancelled()
        {
            gameFieldManager.Hide();
            Show();
        }

        public void Show()
        {
            Debug.Log($"{nameof(NewTriggerMenu)}.{nameof(Show)}");
            gameObject.SetActive(true);
        }

        private void Hide()
        {
            Debug.Log($"{nameof(NewTriggerMenu)}.{nameof(Hide)}");
            gameObject.SetActive(false);
        }
    }
}