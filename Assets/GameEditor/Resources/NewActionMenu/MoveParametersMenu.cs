using System.Globalization;
using GameEditor.Scripts;
using UnityEngine;
using UnityEngine.UI;
namespace GameEditor.Resources.NewActionMenu
{
    public class MoveParametersMenu : MonoBehaviour
    {
        public InputField timeInputField;

        public Button backButton;
        public Button saveButton;

        public NewActionMenu newActionMenu;
        public GameFieldManager gameField;

        private double selectedAngle;
        private double selectedDistance;

        private void Start()
        {
            backButton.onClick.AddListener(() =>
            {
                Hide();
                newActionMenu.ShowActionTypeSelectionMenu();
            });

            saveButton.onClick.AddListener(() =>
                {
                    Hide();
                    newActionMenu.SetMoveParameters(
                        selectedAngle,
                        selectedDistance,
                        double.Parse(
                            timeInputField.text.Replace(',', '.'),
                            NumberStyles.Any,
                            CultureInfo.InvariantCulture
                        )
                    );
                }
            );
        }

        private void OnMoveVectorSelected(double angle, double distance)
        {
            Debug.Log($"{nameof(NewTriggerMenu)}.{nameof(OnMoveVectorSelected)}: "
                + $"angle={angle:f1}, distance={distance:f1}");

            gameField.Hide();
            Show();

            selectedAngle = angle;
            selectedDistance = distance;
        }

        private void OnSelectionCancelled()
        {
            Debug.Log($"{nameof(NewTriggerMenu)}.{nameof(OnSelectionCancelled)}");
            newActionMenu.Show();
        }

        public void Show(int targetId)
        {
            Debug.Log($"{nameof(MoveParametersMenu)}.{nameof(Show)}: targetId={targetId}");

            gameField.Show();
            gameField.EnterMoveVectorSelectionMode(
                targetId,
                OnMoveVectorSelected,
                OnSelectionCancelled
            );
        }

        public void Show()
        {
            Debug.Log($"{nameof(MoveParametersMenu)}.{nameof(Show)}");
            gameObject.SetActive(true);
        }
        public void Hide()
        {
            Debug.Log($"{nameof(MoveParametersMenu)}.{nameof(Hide)}");
            gameObject.SetActive(false);
        }
    }
}