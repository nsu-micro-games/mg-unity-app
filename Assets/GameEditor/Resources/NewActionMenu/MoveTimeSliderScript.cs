using UnityEngine;
using UnityEngine.UI;

namespace GameEditor.Resources.NewActionMenu
{
    public class MoveTimeSliderScript : MonoBehaviour
    {
        public Text value;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        public void SetVisible()
        {
            gameObject.SetActive(!gameObject.activeSelf);
        }

        // Update is called once per frame
        void Update()
        {
            value.text = gameObject.GetComponent<UnityEngine.UI.Slider>().value.ToString("0.0");
        }
    }
}
