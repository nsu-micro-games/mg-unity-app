using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameEditor.Resources.NewActionMenu
{
    public class MovePropsScript : MonoBehaviour
    {

        public LegacyObjectMenuScript objectMenu; 
        public GameObject TimeSlider;
        public Editor editor;

        public float angle;
        public float distance;
        public int objId;
        public float time;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        public void SetMoveTime()
        {
            time = TimeSlider.GetComponent<Slider>().value;
        }

        public void AddMoveAction()
        {
            List<double> props = new List<double> {angle, distance, time};
            editor.AddAction(1, props);
        }

        // Update is called once per frame
        void Update()
        {
            objId = objectMenu.selectedObjectId;   
        }
    }
}
