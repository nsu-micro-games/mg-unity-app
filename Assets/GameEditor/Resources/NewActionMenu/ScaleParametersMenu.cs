using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
namespace GameEditor.Resources.NewActionMenu
{
    public class ScaleParametersMenu : MonoBehaviour
    {
        public InputField scaleInputField;
        public InputField timeInputField;

        public Button backButton;
        public Button saveButton;

        public NewActionMenu newActionMenu;

        private void Start()
        {
            backButton.onClick.AddListener(() =>
            {
                Hide();
                newActionMenu.ShowActionTypeSelectionMenu();
            });

            saveButton.onClick.AddListener(() =>
            {
                Hide();
                newActionMenu.SetScaleParameters(
                    scale: double.Parse(
                        scaleInputField.text.Replace(',', '.'),
                        NumberStyles.Any,
                        CultureInfo.InvariantCulture
                    ),
                    duration: double.Parse(
                        timeInputField.text.Replace(',', '.'),
                        NumberStyles.Any,
                        CultureInfo.InvariantCulture
                    )
                );
            });
        }

        public void Show()
        {
            Debug.Log($"{nameof(ScaleParametersMenu)}.{nameof(Show)}");
            gameObject.SetActive(true);
        }
        public void Hide()
        {
            Debug.Log($"{nameof(ScaleParametersMenu)}.{nameof(Hide)}");
            gameObject.SetActive(false);
        }
    }
}