using GameEditor.Scripts;
using UnityEngine;
using UnityEngine.UI;
namespace GameEditor.Resources.NewActionMenu
{
    public class NewActionMenu : MonoBehaviour
    {
        public GameObject topNavigationPanel;
        public GameObject actionTypeSelectionMenu;

        public Button typeRemoveButton;

        public ScaleParametersMenu scaleParametersMenu;
        public Button typeScaleButton;

        public MoveParametersMenu moveParametersMenu;
        public Button typeMoveButton;

        public Button backButton;

        public EventsMenu.EventsMenu eventsMenu;
        public GameFieldManager gameField;

        private int targetObjectId;

        private void Start()
        {
            backButton.onClick.AddListener(() =>
            {
                Debug.Log($"{nameof(NewActionMenu)}: Back");
                Hide();
                eventsMenu.Show();
            });

            typeRemoveButton.onClick.AddListener(() =>
            {
                Hide();
                gameField.Show();
                gameField.EnterObjectSelectionMode(OnObjectToRemoveSelected, OnSelectionCancelled);
            });

            typeScaleButton.onClick.AddListener(() =>
            {
                Hide();
                gameField.Show();
                gameField.EnterObjectSelectionMode(OnObjectToScaleSelected, OnSelectionCancelled);
            });

            typeMoveButton.onClick.AddListener(() =>
            {
                Hide();
                gameField.Show();
                gameField.EnterObjectSelectionMode(OnObjectToMoveSelected, OnSelectionCancelled);
            });
        }

        private void OnObjectToRemoveSelected(int id)
        {
            targetObjectId = id;

            Debug.Log($"{nameof(NewTriggerMenu)}.{nameof(OnObjectToRemoveSelected)}: id={targetObjectId}");

            gameField.Hide();

            eventsMenu
                .SetRemoveParameters(id)
                .Show();
        }

        private void OnObjectToScaleSelected(int id)
        {
            targetObjectId = id;

            Debug.Log($"{nameof(NewTriggerMenu)}.{nameof(OnObjectToRemoveSelected)}: id={targetObjectId}");

            gameField.Hide();
            Show();
            HideActionTypeSelectionMenu();
            scaleParametersMenu.Show();
        }

        private void OnObjectToMoveSelected(int id)
        {
            targetObjectId = id;

            Debug.Log($"{nameof(NewTriggerMenu)}.{nameof(OnObjectToMoveSelected)}: id={targetObjectId}");
            
            gameField.Hide();
            Show();
            HideActionTypeSelectionMenu();
            moveParametersMenu.Show(id);
        }

        private void OnSelectionCancelled()
        {
            Debug.Log($"{nameof(NewTriggerMenu)}.{nameof(OnSelectionCancelled)}");

            gameField.Hide();
            Show();
        }

        public void SetScaleParameters(double scale, double duration)
        {
            Debug.Log($"{nameof(NewActionMenu)}.{nameof(SetScaleParameters)}: scale={scale}, duration={duration}");

            Hide();
            eventsMenu
                .SetScaleParameters(targetObjectId, scale, duration)
                .Show();
        }

        public void SetMoveParameters(double angle, double distance, double duration)
        {
            Debug.Log($"{nameof(NewTriggerMenu)}.{nameof(SetMoveParameters)}: id={targetObjectId}, "
                + $"angle={angle:f1}, distance={distance:f1}");
            
            gameField.Hide();
            eventsMenu
                .SetMoveParameters(targetObjectId, angle, distance, duration)
                .Show();
        }

        public void ShowActionTypeSelectionMenu()
        {
            topNavigationPanel.SetActive(true);
            actionTypeSelectionMenu.SetActive(true);
        }

        private void HideActionTypeSelectionMenu()
        {
            topNavigationPanel.SetActive(false);
            actionTypeSelectionMenu.SetActive(false);
        }

        public void Show()
        {
            Debug.Log($"{nameof(NewActionMenu)}.{nameof(Show)}");
            gameObject.SetActive(true);
            
            ShowActionTypeSelectionMenu();
        }
        public void Hide()
        {
            Debug.Log($"{nameof(NewActionMenu)}.{nameof(Hide)}");
            gameObject.SetActive(false);
        }
    }
}