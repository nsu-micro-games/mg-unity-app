using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class MoveTimeSliderScript : MonoBehaviour
{
    public Text value;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void SetVisible()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

    // Update is called once per frame
    void Update()
    {
        value.text = gameObject.GetComponent<UnityEngine.UI.Slider>().value.ToString("0.0");
    }
}
