using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScalePropsScript : MonoBehaviour
{
    public Editor editor;
    public void AddAction()
    {
        List<double> props = new List<double>();
        for(int i = 1; i <= 2; i++)
        {
           var propInput = transform.Find("ParamInput" + i);
           props.Add(double.Parse(propInput.gameObject.GetComponent<InputField>().text));
        }
        editor.AddAction(2, props);
    }
}
