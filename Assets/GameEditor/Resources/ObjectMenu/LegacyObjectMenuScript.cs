using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class LegacyObjectMenuScript : MonoBehaviour
{
    private int selectedTriggerId;
    private int selectedActionId;
    private List<double> actionParams;
    private string selectedSoundName;

    [FormerlySerializedAs("selectedObjejctId")]
    public int selectedObjectId;

    public GameObject TriggerChoice;
    public Text TriggerSelectedText;
    public Text TriggerSelectedId;

    public GameObject ActionChoice;
    public Text ActionSelectedText;
    public Text ActionSelectedId;

    public Toggle ForWinToggle;

    public Button ApplyButton;
    public Button BackButton;

    public Button selectSoundButton;
    public Text SoundSelectedName;

    public Button confirmDeleteButton;

    public Editor editor;

    // Start is called before the first frame update
    void Start()
    {
        ApplyButton.onClick.AddListener(AddEvent);
        // confirmDeleteButton.onClick.AddListener(DeleteObject);
    }

    public void SetSelectedTrigger(int id)
    {
        selectedTriggerId = id;
        TriggerSelectedId.text = id.ToString();

        SetTriggerChoosed(true);
    }

    public void SetSelectedActionId(int id)
    {
        selectedActionId = id;
        ActionSelectedId.text = id.ToString();

        SetActionChoosed(true);
    }

    public void SetSelectedSoundName(string name)
    {
        selectedSoundName = name;
        SoundSelectedName.text = name;
        SetSoundChoosed(true);
    }

    void AddEvent()
    {
        GameDAO.MGEventDAO _event = new GameDAO.MGEventDAO
        {
            triggerId = selectedTriggerId,
            actionId = selectedActionId,
            sound = selectedSoundName,
            objectId = selectedObjectId
        };

        editor.AddEvent(_event, ForWinToggle.isOn);
        SetSoundChoosed(false);
        BackButton.onClick.Invoke();
    }

    void SetTriggerChoosed(bool isChoosed)
    {
        if (isChoosed == true)
        {
            TriggerChoice.SetActive(false);
            TriggerSelectedText.gameObject.SetActive(true);
        } else
        {
            TriggerChoice.SetActive(true);
            TriggerSelectedText.gameObject.SetActive(false);
        }
    }

    void SetActionChoosed(bool isChoosed)
    {
        if (isChoosed == true)
        {

            ActionChoice.SetActive(false);
            ActionSelectedText.gameObject.SetActive(true);
        } else
        {

            ActionChoice.SetActive(true);
            ActionSelectedText.gameObject.SetActive(false);
        }
    }

    void SetSoundChoosed(bool isShoosed)
    {
        if (isShoosed)
        {
            selectSoundButton.gameObject.SetActive(false);
            SoundSelectedName.gameObject.SetActive(true);
        } else
        {
            SoundSelectedName.text = "";
            selectedSoundName = "";
            selectSoundButton.gameObject.SetActive(true);
            SoundSelectedName.gameObject.SetActive(false);
        }
    }

    public void ShowObjectMenu(int objectId)
    {
        selectedObjectId = objectId;

        gameObject.SetActive(true);

        ApplyButton.gameObject.SetActive(false);
        SetActionChoosed(false);
        SetTriggerChoosed(false);
        SetSoundChoosed(false);
    }

    public void DeleteObject()
    {
        editor.DeleteObject(selectedObjectId);
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ActionSelectedText.gameObject.activeSelf == true
            && TriggerSelectedText.gameObject.activeSelf == true
            && ApplyButton.gameObject.activeSelf == false)
        {
            ApplyButton.gameObject.SetActive(true);
        }
    }
}