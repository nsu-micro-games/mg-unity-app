using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundMenuScript : MonoBehaviour
{
    public Transform soundsList;
    public Button backButton;
    public LegacyObjectMenuScript menuScript;

    // Start is called before the first frame update
    void Start()
    {
        var soundSources = Resources.LoadAll<AudioClip>("Sounds/Effects");
        foreach(var sound in soundSources)
        {
            GameObject soundItem = Resources.Load<GameObject>("ObjectMenu/SelectSoundMenu/SoundListItem");
            soundItem.GetComponent<SoundListItemScript>().clip = sound;
            soundItem.GetComponent<SoundListItemScript>().soundName.text = sound.name;
            Instantiate(soundItem).transform.SetParent(soundsList);
        }
    }

    public void SetAudioClip(AudioClip clip)
    {
        gameObject.GetComponent<AudioSource>().Stop();
        gameObject.GetComponent<AudioSource>().clip = clip;
        gameObject.GetComponent<AudioSource>().Play();
    }

    public void AddAudio(string audioName)
    {
        menuScript.SetSelectedSoundName(audioName);
        backButton.onClick.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
