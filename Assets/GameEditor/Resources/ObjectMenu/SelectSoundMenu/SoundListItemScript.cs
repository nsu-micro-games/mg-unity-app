using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundListItemScript : MonoBehaviour
{
    public AudioClip clip;
    public Text soundName;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void playAudio()
    {
        transform.GetComponentInParent<SoundMenuScript>().SetAudioClip(clip);
    }

    public void setAudio()
    {
        transform.GetComponentInParent<SoundMenuScript>().AddAudio(clip.name);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
