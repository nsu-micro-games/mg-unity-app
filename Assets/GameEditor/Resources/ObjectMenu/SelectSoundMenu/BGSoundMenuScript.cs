using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class BGSoundMenuScript : MonoBehaviour
{
    public Transform soundsList;
    public Button backButton;
    public Editor editor;

    // Start is called before the first frame update
    void Start()
    {
        var soundSources = Resources.LoadAll<AudioClip>("Sounds/Background");
        foreach(var sound in soundSources)
        {
            GameObject soundItem = Resources.Load<GameObject>("ObjectMenu/SelectSoundMenu/BGSoundListItem");
            soundItem.GetComponent<BGSoundListItemScript>().clip = sound;
            soundItem.GetComponent<BGSoundListItemScript>().soundName.text = sound.name;
            Instantiate(soundItem).transform.SetParent(soundsList);
        }
    }

    public void SetAudioClip(AudioClip clip)
    {
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().clip = clip;
        GetComponent<AudioSource>().Play();
    }

    public void AddAudio(string audioName)
    {
        editor.bgMusic = audioName;
        backButton.onClick.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
