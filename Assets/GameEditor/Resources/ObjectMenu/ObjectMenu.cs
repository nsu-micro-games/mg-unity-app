using GameEditor.Scripts;
using UnityEngine;
using UnityEngine.UI;
namespace GameEditor.Resources.ObjectMenu
{
    public class ObjectMenu : MonoBehaviour
    {
        public Button backButton;
        public Button deleteButton;
        
        public GameObject confirmDeleteMenu;
        public Button confirmDeleteBackButton;
        public Button confirmDeleteButton;

        public Editor editor;
        public GameFieldManager gameField;
        
        private int selectedObjectId;

        private void Start()
        {
            backButton.onClick.AddListener(() =>
            {
                Hide();
                ShowEditor();
            });
            
            deleteButton.onClick.AddListener(() =>
            {
                Hide();
                confirmDeleteMenu.SetActive(true);
            });
            
            confirmDeleteBackButton.onClick.AddListener(() =>
            {
                confirmDeleteMenu.SetActive(false);
                ShowEditor();
            });
            
            confirmDeleteButton.onClick.AddListener(() =>
            {
                editor.DeleteObject(selectedObjectId);
                confirmDeleteMenu.SetActive(false);
                
                ShowEditor();
            });
        }
        
        public void Show(int objectId)
        {
            Debug.Log($"{nameof(ObjectMenu)}.{nameof(Show)}: objectId={objectId}");

            selectedObjectId = objectId;
            gameObject.SetActive(true);
        }

        private void Hide()
        {
            Debug.Log($"{nameof(ObjectMenu)}.{nameof(Hide)}");
            gameObject.SetActive(false);
        }

        private void ShowEditor()
        {
            editor.Show();
            gameField.Show();
        }
    }
}