using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePanelManager : MonoBehaviour
{
    Toggle BinButton;
    // Start is called before the first frame update
    void Start()
    {
        BinButton = GameObject.Find("BinButton").GetComponent<Toggle>();    
    }

    public void Choosed()
    {
        if (BinButton.isOn == true)
        {
            DeleteGame();
        }
        else
        {
            LoadGame();
        }
    }
    void LoadGame()
    {
       GameEditorLoader.LoadGameToEditor(transform.GetComponentInChildren<Text>().text);       
    }

    void DeleteGame()
    {
        GameEditorLoader.DeleteGame(transform.GetComponentInChildren<Text>().text, gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
