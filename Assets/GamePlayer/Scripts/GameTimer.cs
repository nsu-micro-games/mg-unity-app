using UnityEngine;

public class GameTimer : MonoBehaviour
{
    private static double startTime;

    public static double StartTime
    {
        get { return startTime; }
        set { startTime = value; }
    }

    private bool isTimerActive = false;

    private double timeLeft;

    private int nextUpdate;

    // Start is called before the first frame update
    private void Start()
    {
        EventManager.StartListening("StartGameTimer", StartTimer);
        EventManager.StartListening("StopGameTimer", StopTimer);
    }

    private void StartTimer()
    {
        isTimerActive = true;
        timeLeft = startTime;
    }

    private void StopTimer()
    {
        isTimerActive = false;
    }

    // Update is called once per frame
    private void Update()
    {
        if (isTimerActive)
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 1)
            {
                Debug.Log("Game Timer is left!");
                StopTimer();
                EventManager.TriggerEvent("GameTimerLeft");
            }
            else
            {
                if (Time.time >= nextUpdate)
                {
                    nextUpdate = Mathf.FloorToInt(Time.time) + 1;
                    Debug.Log((int) timeLeft + "...");
                }
            }
        }
    }
}