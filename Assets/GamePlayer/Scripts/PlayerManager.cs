using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{

    public static bool isPlaying = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void StartPlayer()
    {
        isPlaying = true;
        ScoreManager.NullifyScore();
        PlayerPrefs.SetInt("SavedScene", SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadSceneAsync("GameScene", LoadSceneMode.Single);
    }

    public static void FinishPlayer()
    {
        isPlaying = false;
        SceneManager.LoadSceneAsync(PlayerPrefs.GetInt("SavedScene"), LoadSceneMode.Single);
    }
}
