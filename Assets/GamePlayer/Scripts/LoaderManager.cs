using GamePlayer.Scripts;
using UnityEngine;

public class LoaderManager : MonoBehaviour
{
    private GameData gameData;
    private string json;

    public static int PUBLISH_BUILD_INDEX = 9;
    public static int EDITOR_BUILD_INDEX = 11;

    public RestClient restClient;

    // Start is called before the first frame update
    private void Start()
    {
        Debug.Log("Loading...");
        LoadGame();
    }

    private void LoadGame()
    {
        restClient.GetRequest<UserData>(
            DataHolder.BackEndAddress + "/api/profiles/getUser",
            GetUserDataCompleted,
            AuthorizationManager.Token
        );
        Debug.Log($"{nameof(LoaderManager)}.{nameof(LoadGame)} Issued GET /api/profiles/getUser");
    }

    private void GetUserDataCompleted(bool ok, string unused, UserData userData)
    {
        Debug.Log(
            $"{nameof(LoaderManager)}.{nameof(GetUserDataCompleted)} "
            + $"GET /api/profiles/getUser completed, ok={ok}"
        );
        var savedSceneIndex = PlayerPrefs.GetInt("SavedScene");

        if (savedSceneIndex == PUBLISH_BUILD_INDEX)
        {
            if (GameManager.isGamePassed)
            {
                PlayerManager.FinishPlayer();
                return;
            }
        }

        GameManager.gamesQueue.TryDequeue(out gameData);
        GameManager.gameId = gameData.id;
        Debug.Log($"{nameof(LoaderManager)}.{nameof(LoadGame)} {nameof(gameData.authorId)}={gameData.authorId}");
        GameManager.ownGame =
            ok && (gameData.authorId == userData.id || gameData.authorId == GameData.LocalAuthorID);

        if (savedSceneIndex == (EDITOR_BUILD_INDEX | PUBLISH_BUILD_INDEX))
        {
            GameManager.gamesQueue.Enqueue(gameData);
        }

        GameManager.gameDao = gameData.gameJson;

        GameManager.gameDataIsReceived = true;

        Debug.Log("Game data received successfully!");
        EventManager.TriggerEvent("GameDataIsReceived");
    }
}