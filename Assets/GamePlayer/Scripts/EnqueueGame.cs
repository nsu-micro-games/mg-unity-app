using UnityEngine;

public class EnqueueGame : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameManager.Init();
        GameManager.EnqueueGames();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}