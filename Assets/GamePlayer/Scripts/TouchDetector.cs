using System.Linq;
using UnityEngine;

public class TouchDetector : MonoBehaviour
{
    private void OnMouseDown()
    {
        if (false == GameManager.playing)
        {
            return;
        }

        var enabledTriggers = GameManager.triggers.Values
            .Where(it => it.IsEnabled && it.GameObj == gameObject);

        foreach (var trigger in enabledTriggers)
        {
            trigger.IsTriggered = true;
        }
    }
}