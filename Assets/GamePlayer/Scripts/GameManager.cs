using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GamePlayer.Scripts;
using SharedScripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static bool gameDataIsReceived;
    private static bool gameLoaded;

    public static bool playing;

    private static ConcurrentDictionary<int, GameObject> objects;
    public static ConcurrentDictionary<int, Trigger> triggers;
    private static ConcurrentDictionary<int, Action> actions;
    private static ConcurrentDictionary<int, Event> events;
    private static List<Event> eventsToWin;

    public static ConcurrentQueue<GameData> gamesQueue;

    public static GameDAO gameDao;
    public static long gameId;
    public static bool ownGame;

    private GameObject gameField;
    private GameObject background;

    public AudioSource audioSource;
    public AudioSource audioSourceEffects;
    public AudioClip audioBackground;

    public static bool isGamePassed;

    private GameObject customObjectTemplate;

    public static GameManager instance;

    public static void Init()
    {
        gamesQueue = new ConcurrentQueue<GameData>();
    }

    public static void ClearGamesQueue()
    {
        GameData gd;
        while (!gamesQueue.IsEmpty)
        {
            gamesQueue.TryDequeue(out gd);
        }
    }

    private void Awake()
    {
        instance = this;

        objects = new ConcurrentDictionary<int, GameObject>();
        triggers = new ConcurrentDictionary<int, Trigger>();
        actions = new ConcurrentDictionary<int, Action>();
        events = new ConcurrentDictionary<int, Event>();
        eventsToWin = new List<Event>();

        gameField = GameObject.Find("GameField");
        background = GameObject.Find("Background");
    }

    // Start is called before the first frame update
    private void Start()
    {
        customObjectTemplate = Resources.Load<GameObject>("Prefabs/UserObjectTemplate");

        audioSource.volume = DataHolder.MusicVolume;
        audioSourceEffects.volume = DataHolder.SoundVolume;
        LaunchLoaderScene();
    }

    // Update is called once per frame
    private void Update()
    {
        if (playing)
        {
            foreach (var ev in events.Values)
            {
                if (false == ev.IsExecuted && ev.IsTriggered && ev.ActionTarget != null)
                {
                    ev.Execute(audioSourceEffects);
                }
            }

            var isWin = eventsToWin.Aggregate(
                eventsToWin.Count > 0,
                (current, evToWin) => current && evToWin.IsActionCompleted && evToWin.NoRepeatsLeft
            );

            if (isWin)
            {
                EventManager.TriggerEvent("Win");
            }
        }

        StartCoroutine(GamesLoader.LoadNewGamesIfQueueIsEmpty());
    }

    public static void EnqueueGames()
    {
        var gameData = new GameData();
        gameData.gameJson = JsonUtility.FromJson<GameDAO>(
            File.ReadAllText("Assets/Game/GameJson.json"));
        gamesQueue.Enqueue(gameData);
    }

    private void LaunchScore()
    {
        SceneManager.LoadSceneAsync("ScoreScene", LoadSceneMode.Additive);

        Debug.Log("Win: " + ScoreManager.winScore + " Lose: " + ScoreManager.loseScore);

        IEnumerator Score()
        {
            yield return new WaitForSeconds(2f);
            LaunchLoaderScene();
            SceneManager.UnloadSceneAsync("ScoreScene");
        }

        StartCoroutine(Score());
    }

    private void LaunchLoaderScene()
    {
        if (gamesQueue.IsEmpty)
        {
            PlayerManager.FinishPlayer();
            return;
        }

        SceneManager.LoadSceneAsync("LoaderScene", LoadSceneMode.Additive);

        EventManager.StartListening("LoaderTimerLeft", CheckLoadingCondition);
        EventManager.StartListening("GameDataIsReceived", GameLoading);

        IEnumerator TriggerLater() // Temporal solution... Sometimes trigger before StartListening. How to fix?
        {
            yield return new WaitForSeconds(1f);
            EventManager.TriggerEvent("StartLoaderTimer");
        }

        StartCoroutine(TriggerLater());
    }

    private void CloseLoaderScene()
    {
        EventManager.StopListening("LoaderTimerLeft", CheckLoadingCondition);
        SceneManager.UnloadSceneAsync("LoaderScene");
    }

    private void CheckLoadingCondition()
    {
        IEnumerator IsLoad()
        {
            while (!(gameDataIsReceived && gameLoaded))
            {
                yield return null;
            }
        }

        StartCoroutine(IsLoad());
        StartGame();
    }

    private void StartGame()
    {
        CloseLoaderScene();
        Debug.Log("Game!!!");
        playing = true;

        audioSource.clip = audioBackground;
        audioSource.Play();

        EventManager.StartListening("GameTimerLeft", TimerLeft);
        EventManager.StartListening("Win", Win);
        EventManager.StartListening("Lose", Lose);

        EventManager.TriggerEvent("StartGameTimer");
    }

    private void FinishGame()
    {
        EventManager.StopListening("GameTimerLeft", TimerLeft);
        EventManager.StopListening("Win", Win);
        EventManager.StopListening("Lose", Lose);

        playing = false;
        Debug.Log("Finish...");

        ClearGameScene();

        var savedSceneIndex = PlayerPrefs.GetInt("SavedScene");

        if (savedSceneIndex == (LoaderManager.EDITOR_BUILD_INDEX | LoaderManager.PUBLISH_BUILD_INDEX))
        {
            LaunchLoaderScene();
        } else
        {
            LaunchScore();
        }
    }

    private void Win()
    {
        Debug.Log("Win!!!");
        EventManager.TriggerEvent("StopGameTimer");
        ScoreManager.Win();
        isGamePassed = true;
        FinishGame();
    }

    private void Lose()
    {
        Debug.Log("Lose...");
        EventManager.TriggerEvent("StopGameTimer");
        ScoreManager.Lose();
        FinishGame();
    }

    private void TimerLeft()
    {
        Debug.Log("Lose... (GameTimer Left)");
        ScoreManager.Lose();
        FinishGame();
    }

    private GameObject LoadGameObject(GameDAO.MGObjectDAO objectDao)
    {
        if (false == objectDao.hasLocalGraphics)
        {
            return Instantiate(
                Resources.Load<GameObject>("Prefabs/" + objectDao.prefabName),
                new Vector3(objectDao.x, objectDao.y),
                Quaternion.identity
            );
        }

        var obj = Instantiate(
            customObjectTemplate,
            new Vector3(objectDao.x, objectDao.y),
            Quaternion.identity
        );

        if (false == ownGame)
        {
            Debug.Log(
                $"{nameof(GameManager)}.{nameof(LoadGameObject)} "
                + $"Object has local graphics but the game was not created by the current author"
                );
            
            return obj;
        }

        var sprite = SpriteUtility.LoadFromImage($"{Application.persistentDataPath}/UserSprites/{objectDao.spriteName}");
        if (null == sprite)
        {
            Debug.Log(
                $"{nameof(GameManager)}.{nameof(LoadGameObject)} \"{objectDao.spriteName}\" does not exist locally, "
                + "falling back to the default sprite"
            );
            
            return obj;
        }
        obj.GetComponent<SpriteRenderer>().sprite = sprite;
        obj.GetComponent<CircleCollider2D>().radius =
            Math.Min(sprite.bounds.extents.x, sprite.bounds.extents.y);

        return obj;
    }

    private void GameLoading()
    {
        EventManager.StopListening("GameDataIsReceived", GameLoading);

        background.GetComponent<Image>().sprite =
            Resources.Load<Sprite>("Art/Background/" + gameDao.backgroundImage);
        objects.TryAdd(0, background);


        foreach (var objDao in gameDao.objects)
        {
            var obj = LoadGameObject(objDao);
            obj.AddComponent<AudioSource>();
            obj.transform.parent = gameField.transform;
            objects.TryAdd(objDao.id, obj);

            Debug.Log($"loaded object with id={objDao.id}");
        }

        Action action;
        foreach (var actionDao in gameDao.actions)
        {
            switch(actionDao.type)
            {
                case 1:
                    action = new MoveAction(actionDao.param1, actionDao.param2, actionDao.param3);
                    actions.TryAdd(actionDao.id, action);
                    break;
                case 2:
                    action = new ScaleAction(actionDao.param1, actionDao.param2);
                    actions.TryAdd(actionDao.id, action);
                    break;
                case 3:
                    action = new RemoveAction();
                    actions.TryAdd(actionDao.id, action);
                    break;
            }

            Debug.Log($"loaded action with id={actionDao.id}, type={actionDao.type}");
        }

        foreach (var triggerDao in gameDao.triggers)
        {
            if (objects.TryGetValue(triggerDao.objectId, out var obj))
            {
                triggers.TryAdd(triggerDao.id, new TapTrigger(obj));
            } else
            {
                Debug.LogWarning($"Object with id {triggerDao.objectId} was not present (trigger {triggerDao.id})");
            }

            Debug.Log($"loaded trigger with id={triggerDao.id}, object_id={triggerDao.objectId}");
        }

        foreach (var eventDao in gameDao.events)
        {
            var audioEffect = (AudioClip) Resources.Load("Sounds/Effects/" + eventDao.sound);
            events.TryAdd(eventDao.id, new Event(
                objects[eventDao.objectId],
                triggers[eventDao.triggerId],
                actions[eventDao.actionId],
                audioEffect,
                eventDao.repeats
            ));

            Debug.Log($"loaded event with id={eventDao.id}, "
                + $"sound_name={eventDao.sound}, "
                + $"object_id={eventDao.objectId}, "
                + $"trigger_id={eventDao.triggerId}, "
                + $"action_id={eventDao.actionId}, "
                + $"repeats={eventDao.repeats}");
        }

        foreach (var id in gameDao.eventForWinIds)
        {
            if (false == events.TryGetValue(id, out var ev))
            {
                Debug.LogError($"Event for win with id={id} is not in events");
            }
            eventsToWin.Add(ev);
        }

        audioBackground = (AudioClip) Resources.Load("Sounds/Background/" + gameDao.backgroundMusic);
        audioSource.loop = true;

        GameTimer.StartTime = gameDao.timeout;
        gameLoaded = true;

        Debug.Log("Game loaded successfully!");
    }

    private void ClearGameScene()
    {
        foreach (var objValue in objects.Values)
        {
            if (!objValue.Equals(background))
            {
                Destroy(objValue);
            }
        }

        eventsToWin.Clear();
        events.Clear();
        actions.Clear();
        triggers.Clear();
        objects.Clear();

        gameDataIsReceived = false;
        gameLoaded = false;
    }

    public abstract class Trigger
    {
        public bool IsEnabled {get; set;} = true;
        public bool IsTriggered {get; set;}

        public GameObject GameObj {get;}

        protected Trigger(GameObject obj)
        {
            GameObj = obj;
        }
    }

    private class TapTrigger : Trigger
    {
        public TapTrigger(GameObject obj) : base(obj)
        {
        }
    }

    private abstract class Action
    {
        public System.Action completed = () => {};

        public abstract void DoAction(GameObject actionTarget);

        protected abstract void Reset();

        protected Action() => completed += Reset;
    }

    private class RemoveAction : Action
    {
        public override void DoAction(GameObject gameObj)
        {
            if (gameObj != null)
            {
                Destroy(gameObj);
            }
            completed();
        }

        protected override void Reset() {}
    }

    private class ScaleAction : Action
    {
        private readonly float scaleFactor;
        private float scalingProgress;

        private readonly double timeToScale;
        private double timeLeft;

        public ScaleAction(double scaleFactor, double timeToScale)
        {
            this.scaleFactor = (float) scaleFactor;
            this.timeToScale = timeToScale;

            timeLeft = timeToScale;
        }

        public override void DoAction(GameObject gameObj)
        {
            var currScale = gameObj.transform.localScale;
            var startX = currScale.x;
            var startY = currScale.y;

            IEnumerator Scale()
            {
                if (scaleFactor.Equals(1))
                {
                    completed();
                    yield break;
                }

                if (timeToScale.Equals(0))
                {
                    if (gameObj != null)
                    {
                        gameObj.transform.localScale = new Vector3(
                            startX + startX * (scaleFactor - 1),
                            startY + startY * (scaleFactor - 1),
                            0);
                    }

                    completed();
                    yield break;
                }

                while (timeLeft > 0)
                {
                    timeLeft -= Time.deltaTime;
                    scalingProgress = (float) (timeToScale - timeLeft) / (float) timeToScale;

                    if (gameObj != null)
                    {
                        gameObj.transform.localScale = new Vector3(
                            startX + startX * scalingProgress * (scaleFactor - 1),
                            startY + startY * scalingProgress * (scaleFactor - 1),
                            0);
                    } else
                    {
                        yield break;
                    }

                    yield return null;
                }

                completed();
            }

            instance.StartCoroutine(Scale());
        }

        protected override void Reset()
        {
            scalingProgress = 0;
            timeLeft = timeToScale;
        }
    }

    private class MoveAction : Action
    {
        private readonly float angle;
        private readonly float distance;
        private float movingProgress;

        private readonly double timeToMove;
        private double timeLeft;

        public MoveAction(double angle, double distance, double timeToMove)
        {
            this.angle = (float) Math.PI * (float) angle / 180;
            this.distance = (float) distance;

            this.timeToMove = timeToMove;

            timeLeft = timeToMove;
        }

        public override void DoAction(GameObject gameObj)
        {
            var currLocation = gameObj.transform.position;
            var startX = currLocation.x;
            var startY = currLocation.y;

            IEnumerator Move()
            {
                if (distance.Equals(0))
                {
                    completed();
                    yield break;
                }

                if (timeToMove.Equals(0))
                {
                    gameObj.transform.position = new Vector3(
                        startX + distance * (float) Math.Cos(angle),
                        startY + distance * (float) Math.Sin(angle),
                        0);
                    completed();
                    yield break;
                }

                while (timeLeft > 0)
                {
                    timeLeft -= Time.deltaTime;
                    movingProgress = (float) (timeToMove - timeLeft) / (float) timeToMove;

                    if (gameObj != null)
                    {
                        gameObj.transform.position = new Vector3(
                            startX + distance * movingProgress * (float) Math.Cos(angle),
                            startY + distance * movingProgress * (float) Math.Sin(angle),
                            0);
                    } else
                    {
                        yield break;
                    }

                    yield return null;
                }

                completed();
            }

            instance.StartCoroutine(Move());
        }

        protected override void Reset()
        {
            movingProgress = 0.0f;
            timeLeft = timeToMove;
        }
    }

    private class Event
    {
        private readonly Action action;

        private readonly Trigger trigger;

        private readonly AudioClip sound;

        private int repeatsCount;

        public GameObject ActionTarget {get;}

        public Event(
            GameObject actionTarget,
            Trigger trigger,
            Action action,
            AudioClip sound,
            int repeatsCount
        )
        {
            this.action = action;
            this.action.completed += ActionCompletedHandler;

            this.trigger = trigger;
            this.sound = sound;
            this.repeatsCount = repeatsCount;

            ActionTarget = actionTarget;
        }

        public bool IsTriggered => trigger.IsTriggered;

        public bool IsExecuted {get; private set;}

        public bool IsActionCompleted {get; private set;}

        public bool NoRepeatsLeft => repeatsCount <= 0;

        public void Execute(AudioSource fallbackAudioSource)
        {
            IsActionCompleted = false;

            IsExecuted = true;

            trigger.IsTriggered = false;
            trigger.IsEnabled = false;

            if (repeatsCount != GameDAO.MGEventDAO.InfRepeatsCount)
            {
                repeatsCount -= 1;
            }

            action.DoAction(ActionTarget);

            if (ActionTarget != null
                && ActionTarget.TryGetComponent<AudioSource>(out var audioSource)
                && audioSource.enabled)
            {
                audioSource.clip = sound;
                audioSource.Play();
            } else
            {
                fallbackAudioSource.clip = sound;
                fallbackAudioSource.Play();
            }
        }

        private void ActionCompletedHandler()
        {
            IsActionCompleted = true;
            IsExecuted = false;

            var shouldEnable =
                ActionTarget != null
                && (repeatsCount == GameDAO.MGEventDAO.InfRepeatsCount || repeatsCount > 0);
            if (shouldEnable)
            {
                trigger.IsEnabled = true;
            }

            Debug.Log($"${nameof(Event)}.{nameof(ActionCompletedHandler)} "
                + $"{nameof(IsActionCompleted)}={IsActionCompleted} "
                + $"{nameof(trigger.IsEnabled)}={trigger.IsEnabled} "
                + $"{nameof(repeatsCount)}={repeatsCount}");
        }
    }
}