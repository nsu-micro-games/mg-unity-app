using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoaderTimer : MonoBehaviour
{
    private const float StartTime = 5.5f;

    private bool isTimerActive = false;

    private float timeLeft;

    private int nextUpdate;

    // Start is called before the first frame update
    private void Start()
    {
        EventManager.StartListening("StartLoaderTimer", StartTimer);
    }

    private void StartTimer()
    {
        isTimerActive = true;
        timeLeft = StartTime;
    }

    private void StopTimer()
    {
        isTimerActive = false;
    }

    // Update is called once per frame
    private void Update()
    {
        if (isTimerActive)
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 1)
            {
                Debug.Log("Loader Timer is left!");
                StopTimer();
                EventManager.TriggerEvent("LoaderTimerLeft");
            }
            else
            {
                if (Time.time >= nextUpdate)
                {
                    nextUpdate = Mathf.FloorToInt(Time.time) + 1;
                    Debug.Log((int) timeLeft + "...");
                }
            }
        }
    }
}