using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    public static int winScore;
    public static int loseScore;

    public Text winScoreText;
    public Text loseScoreText;

    public static bool isLastGameWin;

    private void Update()
    {
        winScoreText.text = winScore + "";
        loseScoreText.text = loseScore + "";
    }
    public static void Win()
    {
        winScore++;
        isLastGameWin = true;
    }

    public static void Lose()
    {
        loseScore++;
        isLastGameWin = false;
    }

    public static void NullifyScore()
    {
        winScore = 0;
        loseScore = 0;
        // isLastGameWin = false; ?
    }

    public void Like() {
        StartCoroutine(GamesRate.LikeRequest(GameManager.gameId));
    }

    public void Dislike() {
        StartCoroutine(GamesRate.DislikeRequest(GameManager.gameId));
    }

    public void Report() {
        StartCoroutine(GamesRate.ReportRequest(GameManager.gameId, " "));
    }
}