using System;
namespace GamePlayer.Scripts
{
    [Serializable]
    public class GameData
    {
        public const long LocalAuthorID = -1;
        public long id;
        public long authorId;
        public string name;
        public string description;
        public GameDAO gameJson;
        public string[] tags;
        public long[] usersLikedIds;
        public long[] usersDislikedIds;
        public long[] usersReportedIds;

        public GameData() {}

        public GameData(
            long id, long authorId,
            string name, string description,
            GameDAO gameJson, string[] tags,
            long[] usersLikedIds, long[] usersDislikedIds, long[] usersReportedIds
        ) {
            this.id = id;
            this.authorId = authorId;
            this.name = name;
            this.description = description;
            this.gameJson = gameJson;
            this.tags = tags;
            this.usersLikedIds = usersLikedIds;
            this.usersDislikedIds = usersDislikedIds;
            this.usersReportedIds = usersReportedIds;
        }
    }

    [Serializable]
    public class UserData
    {
        public long id;
        public string login;
        public string photoUrl;
        public string bio;

    }
}