using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using TMPro;

public class Publish : MonoBehaviour {

    private TextMeshProUGUI isTestPassedText;
    private GameObject runTestButton;
    private GameObject publishButton;

    public void Start() {
        isTestPassedText = GameObject.Find("IsTestPassedText").GetComponent<TextMeshProUGUI>();
        isTestPassedText.text = DataHolder.IsTestPassed ? "PASSED" : "NOT PASSED";
        runTestButton = GameObject.Find("RunTestButton");
        runTestButton.SetActive(!DataHolder.IsTestPassed);
        publishButton = GameObject.Find("PublishButton");
        publishButton.SetActive(DataHolder.IsTestPassed);
        GameObject.Find("GameTitleIF").GetComponent<TMP_InputField>().text = DataHolder.CurrentlyPublishingGameTitle;
    }

    public void PlusTag() {
        DataHolder.CurrentlyPublishingGameTitle = GameObject.Find("GameTitleIF").GetComponent<TMP_InputField>().text;
        SceneManager.LoadScene("SearchTag", LoadSceneMode.Single);
    }

    public void RunTest() {
        DataHolder.IsTestPassed = true;
        isTestPassedText.text = "PASSED";
        runTestButton.SetActive(false);
        publishButton.SetActive(true);
    }

    public void Back() {
        SceneManager.LoadScene("GameEditorScene", LoadSceneMode.Single);
    }

    public void PublishGame() {
        string title = GameObject.Find("GameTitleIF").GetComponent<TMP_InputField>().text;
        // string gameJson = DataHolder.CurrentGameJson;
        string[] tagsIds = DataHolder.GetSelectedTagsNames();
        // StartCoroutine(PublishGameRequest(title, gameJson, tagsIds));
    }

    private IEnumerator PublishGameRequest(string title, string gameJson, string[] tagsIds) {
        UnityWebRequest uwr = UnityWebRequest.Post(DataHolder.BackEndAddress + "/api/games/create", "");
        string requestJson = JsonUtility.ToJson(new DataHolder.GameSending(title, "", gameJson, tagsIds));
        Debug.Log("Publishing: " + requestJson);
        uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(requestJson));
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.SetRequestHeader("Authorization", "Bearer_" + DataHolder.MyToken);
        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success) {
            Debug.Log(uwr.error);
        } else {
            Debug.Log("Game published successfully");
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }
}