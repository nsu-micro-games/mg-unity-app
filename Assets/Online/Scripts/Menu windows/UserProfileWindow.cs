using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Image = Microsoft.Unity.VisualStudio.Editor.Image;

public class UserProfileWindow : BaseWindow
{
    public Button backButton;
    public Button showMoreGamesButton;
    public Image userAvatarImage;
    public TextMeshProUGUI userNameText;
    public GameObject gamesScrolling;

    private long userId;

    public void Start() {
        DataHolder.Author user = DataHolder.FindAuthorById(DataHolder.IdOfChosenUser);

        userId = user.id;

        userNameText.text = user.login;
        
        // if (user.photoUrl != null) {
        //     StartCoroutine(SetAvatarFromUrl(user.photoUrl, userAvatarImage));
        // }
        //
        // ShowMoreGames();
    }
    
    // public void ShowMoreGames() {
    //     gamesScrolling.SendMessage("ShowMore", userId);
    // }
    //
    // private IEnumerator SetAvatarFromUrl(string url, Image avatar) {
    //     UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url);
    //     yield return uwr.SendWebRequest();
    //
    //     if (uwr.result == UnityWebRequest.Result.ConnectionError) {
    //         Debug.Log(uwr.error);
    //         yield break;
    //     }
    //
    //     var texture = DownloadHandlerTexture.GetContent(uwr);;
    //     var sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), Vector2.one * 0.5f);
    //     avatar.sprite = sprite;
    // }
}