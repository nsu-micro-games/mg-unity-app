using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuWindow : BaseWindow
{
    public Button playButton;
    public Button createButton;
    public Button settingsButton;
    public Button profileButton;
    public TextMeshProUGUI welcomeText;
    
    public void Start() {
        welcomeText.text = "Welcome, " + DataHolder.MyUsername + "!";
        DataHolder.ResetAuthorsAndTags();
    }
}