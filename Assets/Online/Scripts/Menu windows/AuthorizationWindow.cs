using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AuthorizationWindow : BaseWindow
{
    public Button signUpButton;
    public Button signInButton;
    public TMP_InputField usernameInputField;
    public TMP_InputField passwordInputField;
    public WindowManager windowManager;
    public BaseWindow registrationWindow;
    public BaseWindow mainMenuWindow;
    public AuthorizationManager authorizationManager;

    private void Start()
    {
        if (AuthorizationManager.IsAuthorized)
        {
            windowManager.ShowNextWindow(mainMenuWindow);
        }
        signUpButton.onClick.AddListener(SignUp);
        signInButton.onClick.AddListener(SignIn);
    }

    private void SignUp()
    {
        windowManager.ShowNextWindow(registrationWindow);
    }

    private void SignIn()
    {
        var login = usernameInputField.text;
        var password = passwordInputField.text;
        authorizationManager.SignIn(login, password, SignInCallback);
    }

    private void SignInCallback(bool success, string result, DataHolder.LoginResponse response)
    {
        if (!success)
        {
            Debug.Log(result);
        }
        else
        {
            Debug.Log("Authorized successfully, token = " + AuthorizationManager.Token);
            windowManager.ShowNextWindow(mainMenuWindow);
        }
    }
}