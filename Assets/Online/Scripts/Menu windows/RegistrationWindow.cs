using Online.Scripts.Managers;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RegistrationWindow : BaseWindow
{
    public Button backButton;
    public Button signUpButton;
    public TMP_InputField usernameInputField;
    public TMP_InputField passwordInputField;
    public WindowManager windowManager;

    public RegistrationManager registrationManager;

    private void Start()
    {
        signUpButton.onClick.AddListener(SignUp);
        backButton.onClick.AddListener(Back);
    }

    public void Back()
    {
        windowManager.BackToPrevWindow();
    }

    private void SignUp()
    {
        var login = usernameInputField.text;
        var password = passwordInputField.text;
        registrationManager.SignUp(login, password, "", SignUpCallback);
    }

    private void SignUpCallback(bool success, string result, DataHolder.RegistrationResponse response)
    {
        if (!success)
        {
            Debug.Log(result);
        }
        else
        {
            Debug.Log("Registered successfully, token = " + registrationManager.Token);
            windowManager.BackToPrevWindow();
        }
    }
}