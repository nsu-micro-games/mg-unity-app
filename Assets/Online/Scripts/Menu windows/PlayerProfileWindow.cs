using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Online.Scripts.Menu_windows
{
    public class PlayerProfileWindow : BaseWindow
    {
        public Button backButton;
        public Button saveButton;
        public Button avatarButton;
        public Button changePasswordButton;
        public Button logOutButton;
        public TMP_InputField loginField; 
        public TMP_InputField nameField;
        public TMP_InputField bioField;
        public Image avatarImage;
    }
}