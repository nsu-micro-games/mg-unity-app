using UnityEngine;
using UnityEngine.UI;

public class SettingsWindow : BaseWindow
{
    public Button backButton;
    public Button saveButton;
    public Slider musicSlider;
    public Slider soundSlider;

    public void Start() {
        float currentMusicVolume = DataHolder.MusicVolume;
        float currentSoundVolume = DataHolder.SoundVolume;
        musicSlider.value = currentMusicVolume;
        soundSlider.value = currentSoundVolume;
    }
}