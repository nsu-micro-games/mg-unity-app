using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SearchAuthorWindow : BaseWindow
{
    public Button backButton;
    public TMP_InputField searchAuthorField;
    public Button showMoreButton;
}