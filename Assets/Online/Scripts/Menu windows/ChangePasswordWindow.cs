using System;
using TMPro;
using UnityEngine.UI;

namespace Online.Scripts.Menu_windows
{
    public class ChangePasswordWindow : BaseWindow
    {
        public Button cancelButton;
        public Button saveButton;
        public TMP_InputField currentPassword;
        public TMP_InputField newPassword;
        public TMP_InputField confirmNewPassword;
    }
}