using UnityEngine;
using UnityEngine.UI;

public class PlayMenuWindow : BaseWindow
{
    public Button backButton;
    public Button playButton;
    public Button filterButton;
    public Button searchButton;
}