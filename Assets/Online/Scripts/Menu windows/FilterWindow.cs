using UnityEngine.UI;

public class FilterWindow : BaseWindow
{
    public Button backButton;
    public Button plusGameTagButton;
    public Button plusAuthorsButton;
    public Button playButton;
}