using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SearchAuthorButton : MonoBehaviour {
    
    public long AuthorId {get; set;}
    public string AuthorLogin {get; set;}

    public void OnClick() {
        DataHolder.SearchAuthorMode mode = DataHolder.CurrentSearchAuthorMode;
        switch(mode) {
            case DataHolder.SearchAuthorMode.OPEN_PROFILE:
                DataHolder.IdOfChosenUser = AuthorId;
                SceneManager.LoadScene("UserProfile", LoadSceneMode.Single);
                break;
            case DataHolder.SearchAuthorMode.ADD_FILTER:
                DataHolder.SelectAuthor(new DataHolder.Author(AuthorId, AuthorLogin, "", ""));
                SceneManager.LoadScene("Filter", LoadSceneMode.Single);
                break;
        }
    }
}