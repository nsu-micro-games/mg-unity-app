using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SearchAuthor : MonoBehaviour {

    public GameObject authorsScrolling;

    public void Start() {
        if (!DataHolder.AvailableAuthorsPresent()) {
            ShowMore();
        }
    }

    public void ShowMore() {
        authorsScrolling.SendMessage("ShowMore");
    }

    public void Back() {
        DataHolder.SearchAuthorMode mode = DataHolder.CurrentSearchAuthorMode;
        
        switch(mode) {
            case DataHolder.SearchAuthorMode.OPEN_PROFILE:
                SceneManager.LoadScene("PlayMenu", LoadSceneMode.Single);
                break;
            case DataHolder.SearchAuthorMode.ADD_FILTER:
                SceneManager.LoadScene("Filter", LoadSceneMode.Single);
                break;
        }
    }
}