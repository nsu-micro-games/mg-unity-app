using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class SearchAuthorScrolling : MonoBehaviour {

    public GameObject authorButtonPrefab;
    public int buttonsOffset = 5;

    private List<GameObject> buttons;
    private int currentPageNumber = 0;
    private int pageSize = 3;

    public void Start() {
        buttons = new List<GameObject>();
    }
    
    public void ShowMore() {
        StartCoroutine("LoadNextAuthorsPack");
    }

    private IEnumerator LoadNextAuthorsPack() {
        UnityWebRequest uwr = UnityWebRequest.Get(DataHolder.BackEndAddress + "/api/users/list");
        uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(
            JsonUtility.ToJson(new DataHolder.AuthorsFilter("", pageSize, currentPageNumber))));
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.SetRequestHeader("Authorization", "Bearer_" + DataHolder.MyToken);
        yield return uwr.SendWebRequest();

        if (uwr.result == UnityWebRequest.Result.ConnectionError) {
            Debug.Log("Error: " + uwr.error);
        } else {
            string requestResult = uwr.downloadHandler.text;
            Debug.Log("Received: " + requestResult);

            DataHolder.Authors newAuthors = JsonUtility.FromJson<DataHolder.Authors>(
                "{ \"authors\": " + requestResult + "}");
            List<DataHolder.Author> newAuthorsList = new List<DataHolder.Author>(newAuthors.authors);
            DataHolder.AddAvailableAuthors(newAuthorsList);
            InitianizeNewButtons(newAuthorsList);
            currentPageNumber++;
        }
    }

    private void InitianizeNewButtons(List<DataHolder.Author> newAuthors) {
        int buttonsCount = buttons.Count;
        for (int i = buttonsCount; i < buttonsCount + newAuthors.Count; i++) {
            GameObject newButton = Instantiate(authorButtonPrefab, transform, false);
            buttons.Add(newButton);
            SetValuesForButton(newButton, newAuthors[i - buttonsCount]);
            if (i == 0) continue;
            buttons[i].transform.localPosition = new Vector2(
                buttons[i].transform.localPosition.x,
                buttons[i-1].transform.localPosition.y - authorButtonPrefab.GetComponent<RectTransform>().sizeDelta.y);
        }
    }

    private void SetValuesForButton(GameObject button, DataHolder.Author author) {
        SearchAuthorButton component = (SearchAuthorButton)button.GetComponent("SearchAuthorButton");
        component.AuthorId = author.id;
        component.AuthorLogin = author.login;
        button.GetComponentInChildren<TextMeshProUGUI>().text = component.AuthorLogin;
    }
}