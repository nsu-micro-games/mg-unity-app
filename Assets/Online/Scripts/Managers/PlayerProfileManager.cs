using System;
using System.Collections;
using System.IO;
using Online.Scripts.Menu_windows;
using UnityEngine;
using UnityEngine.UI;

namespace Online.Scripts.Managers
{
    public class PlayerProfileManager : MonoBehaviour
    {
        public WindowManager windowManager;
        public BaseWindow changePassword;
        public AuthorizationWindow authorization;

        public PlayerProfileWindow playerProfileWindow;

        public void Start()
        {
            playerProfileWindow.backButton.onClick.AddListener(Back);
            playerProfileWindow.saveButton.onClick.AddListener(Save);
            playerProfileWindow.avatarButton.onClick.AddListener(LoadImage);
            playerProfileWindow.changePasswordButton.onClick.AddListener(ShowChangePassword);
            playerProfileWindow.logOutButton.onClick.AddListener(LogOut);

            playerProfileWindow.loginField.text = DataHolder.MyUsername;
            playerProfileWindow.nameField.text = DataHolder.MyName;  
            playerProfileWindow.bioField.text = DataHolder.MyBio;

            
            playerProfileWindow.avatarImage.GetComponent<Image>().sprite = DataHolder.MyAvatar;
        }
        
        private void LogOut()
        {
            AuthorizationManager.Token = null;
            authorization.usernameInputField.text = "";
            authorization.passwordInputField.text = ""; 
            windowManager.ShowNextWindow(authorization);
        }
        
        public string GetImagePath()
        {
            return UnityEditor.EditorUtility.OpenFilePanel("Choose image", "", "jpg");
        }

        public IEnumerator LoadImageFromDisk(string path)
        {
            var imageData = File.ReadAllBytes(path);
            var texture = new Texture2D(2, 2);
            texture.LoadImage(imageData);
            
            var width = texture.width;
            var height = texture.height;
            
            playerProfileWindow.avatarImage.GetComponent<Image>().sprite = CreateSprite(texture, width, height);
            
            yield return null;
        }

        public Sprite CreateSprite(Texture2D texture, int width, int height)
        {
            Vector2 center = new Vector2(width / 2, height / 2);
            var minSide = Math.Min(width, height);
            var radius = minSide / 2 - 5;
            
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (Vector2.Distance(center, new Vector2(x, y)) > radius )
                    {
                        texture.SetPixel(x, y, Color.white);
                    }
                }
            }
            texture.Apply();

            var requiredWidth = playerProfileWindow.avatarImage.GetComponent<RectTransform>().rect.width;
            
            return Sprite.Create(texture, new Rect((width - minSide) / 2, (height - minSide) / 2, 
                minSide, minSide), new Vector2(0.5f, 0.5f), minSide/requiredWidth);
        }
        
        public void LoadImage()
        {
            string path = GetImagePath();
            if (!string.IsNullOrEmpty(path))
            {
                StartCoroutine(LoadImageFromDisk(path));
            }
        }
        
        public void Back()
        {
            playerProfileWindow.loginField.text = DataHolder.MyUsername;
            playerProfileWindow.nameField.text = DataHolder.MyName;  
            playerProfileWindow.bioField.text = DataHolder.MyBio;

            if (DataHolder.MyAvatar)
                playerProfileWindow.avatarImage.GetComponent<Image>().sprite = DataHolder.MyAvatar;
            else
                playerProfileWindow.avatarImage.GetComponent<Image>().sprite = null;
            
            windowManager.BackToPrevWindow();
        }

        public void ShowChangePassword()
        {
            windowManager.ShowNextWindow(changePassword);
        }

        public void Save()
        {
            DataHolder.MyUsername = playerProfileWindow.loginField.text;
            DataHolder.MyBio = playerProfileWindow.bioField.text;
            DataHolder.MyName = playerProfileWindow.nameField.text;
            
            DataHolder.MyAvatar = playerProfileWindow.avatarImage.GetComponent<Image>().sprite;
        }
    }
}