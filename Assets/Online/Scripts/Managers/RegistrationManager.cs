using System;
using UnityEngine;

namespace Online.Scripts.Managers
{
    public class RegistrationManager : MonoBehaviour
    {
        public string Token;
        public RestClient restClient;

        public void SignUp(string login, string password, string photoUrl, Action<bool, string, DataHolder.RegistrationResponse> callback)
        {
            restClient.PostRequest<DataHolder.RegistrationResponse>(DataHolder.BackEndAddress + "/api/profiles/register",
                new DataHolder.RegistrationDto(login, password, photoUrl),
                (b, s, r) => SignUpCallback(b, s, r, callback));
        }

        private void SignUpCallback(bool success, string result, DataHolder.RegistrationResponse response,
            Action<bool, string, DataHolder.RegistrationResponse> callback)
        {
            if (success)
            {
                Token = response.token;
            }

            callback(success, result, response);
        }
    }
}