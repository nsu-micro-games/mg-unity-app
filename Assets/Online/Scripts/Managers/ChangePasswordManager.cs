using System;
using Online.Scripts.Menu_windows;
using UnityEngine;

namespace Online.Scripts.Managers
{
    public class ChangePasswordManager : MonoBehaviour
    {
        public WindowManager windowManager;

        public ChangePasswordWindow changePasswordWindow;

        public void Start()
        {
            changePasswordWindow.cancelButton.onClick.AddListener(Cancel);
            changePasswordWindow.saveButton.onClick.AddListener(Save);
        }

        public void Cancel()
        {
            windowManager.BackToPrevWindow();
        }

        public void Save()
        {
            Cancel();
        }
    }
}