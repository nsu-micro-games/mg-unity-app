using UnityEngine;

public class FIlterManager : MonoBehaviour
{
    public WindowManager windowManager;
    public BaseWindow searchAuthor;
    public BaseWindow searchTag;
    
    public FilterWindow filterWindow;

    public void Start()
    {
        filterWindow.backButton.onClick.AddListener(Back);
        filterWindow.plusGameTagButton.onClick.AddListener(PlusTag);
        filterWindow.plusAuthorsButton.onClick.AddListener(PlusAuthor);
        filterWindow.playButton.onClick.AddListener(Play);
    }

    public void PlusAuthor()
    {
        windowManager.ShowNextWindow(searchAuthor);
    }

    public void PlusTag()
    {
        windowManager.ShowNextWindow(searchTag);
    }
    
    public void Play() {
        StartCoroutine(GamesLoader.LoadNextGamesPack());
    }
    
    public void Back()
    {
        windowManager.BackToPrevWindow();
    }
}
