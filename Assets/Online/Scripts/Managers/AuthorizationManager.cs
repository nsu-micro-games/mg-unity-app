using System;
using UnityEngine;

public class AuthorizationManager : MonoBehaviour
{
    public static string Token;
    public RestClient restClient;
    public static bool IsAuthorized = false;

    public void SignIn(string login, string password, Action<bool, string, DataHolder.LoginResponse> callback)
    {
        restClient.PostRequest<DataHolder.LoginResponse>(DataHolder.BackEndAddress + "/api/profiles/login",
            new DataHolder.LoginDto(login, password),
            (b, s, r) => SignInCallback(b, s, r, callback));
    }

    private void SignInCallback(bool success, string result, DataHolder.LoginResponse response,
        Action<bool, string, DataHolder.LoginResponse> callback)
    {
        if (success)
        {
            Token = response.token;
            IsAuthorized = true;
        }

        callback(success, result, response);
    }
}