using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    public WindowManager windowManager;

    public Slider musicSlider;
    public Slider soundSlider;

    public SettingsWindow settingsWindow;
    
    public void Start()
    {
        settingsWindow.backButton.onClick.AddListener(Back);
        settingsWindow.saveButton.onClick.AddListener(Save);
    }
    
    public void Back()
    {
        windowManager.BackToPrevWindow();
    }

    public void Save()
    {
        DataHolder.MusicVolume = musicSlider.value;
        DataHolder.SoundVolume = soundSlider.value;
        Back();
    }
}
