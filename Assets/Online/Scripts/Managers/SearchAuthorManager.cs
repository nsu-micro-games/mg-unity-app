using UnityEngine;

public class SearchAuthorManager : MonoBehaviour
{
    public WindowManager windowManager;

    public SearchAuthorWindow searchAuthorWindow;

    public void Start()
    {
        searchAuthorWindow.backButton.onClick.AddListener(Back);
    }

    public void Back()
    {
        windowManager.BackToPrevWindow();
    }
}
