using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public WindowManager windowManager;

    public BaseWindow settings;
    public BaseWindow playMenu;
    public BaseWindow playerProfile;

    public MainMenuWindow mainMenuWindow;

    public void Start()
    {
        mainMenuWindow.playButton.onClick.AddListener(ShowPlayMenu);
        mainMenuWindow.settingsButton.onClick.AddListener(ShowSettings);
        mainMenuWindow.profileButton.onClick.AddListener(ShowProfile);
        mainMenuWindow.createButton.onClick.AddListener(Create);
    }

    private void Create() {
        DataHolder.CurrentSearchTagMode = DataHolder.SearchTagMode.ADD_DURING_PUBLISHING;
        SceneManager.LoadScene("GameEditorLoad", LoadSceneMode.Single);
    }

    public void ShowSettings()
    {
        windowManager.ShowNextWindow(settings);
    }

    public void ShowPlayMenu()
    {
        windowManager.ShowNextWindow(playMenu);
    }

    public void ShowProfile()
    {
        windowManager.ShowNextWindow(playerProfile);
    }
}
