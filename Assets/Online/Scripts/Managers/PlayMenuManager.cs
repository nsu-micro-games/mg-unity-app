using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMenuManager : MonoBehaviour
{
    public WindowManager windowManager;
    public BaseWindow filter;
    public BaseWindow searchAuthor;

    public PlayMenuWindow playMenuWindow;

    public void Start()
    {
        playMenuWindow.backButton.onClick.AddListener(Back);
        playMenuWindow.playButton.onClick.AddListener(Play);
        playMenuWindow.filterButton.onClick.AddListener(ShowFilter);
        playMenuWindow.searchButton.onClick.AddListener(ShowSearchAuthor);
    }

    public void ShowFilter()
    {
        windowManager.ShowNextWindow(filter);
    }

    public void ShowSearchAuthor()
    {
        windowManager.ShowNextWindow(searchAuthor);
    }
    
    public void Play() {
        StartCoroutine(GamesLoader.LoadNextGamesPack());
    }
    
    public void Back()
    {
        windowManager.BackToPrevWindow();
    }
}
