using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddedAuthorButton : MonoBehaviour {

    public long AuthorId {get; set;}
    public string AuthorLogin {get; set;}

    public void OnClick() {
        Destroy(gameObject);
        DataHolder.UnselectAuthor(new DataHolder.Author(AuthorId, AuthorLogin, "", ""));
    }
}