using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddedAuthorsScrolling : MonoBehaviour {

    public GameObject addedAuthorButtonPrefab;
    public int buttonsOffset = 5;

    private GameObject[] buttons;

    public void Start() {
        List<DataHolder.Author> authors = DataHolder.SelectedAuthors;
        buttons = new GameObject[authors.Count];

        for (int i = 0; i < authors.Count; i++) {
            buttons[i] = Instantiate(addedAuthorButtonPrefab, transform, false);
            SetValuesForButton(i, authors[i]);
            if (i == 0) continue;
            buttons[i].transform.localPosition = new Vector2(
                buttons[i-1].transform.localPosition.x + addedAuthorButtonPrefab.GetComponent<RectTransform>().sizeDelta.x,
                buttons[i].transform.localPosition.y);
        }
    }

    private void SetValuesForButton(int buttonIndex, DataHolder.Author author) {
        AddedAuthorButton component = (AddedAuthorButton)buttons[buttonIndex].GetComponent("AddedAuthorButton");
        component.AuthorId = author.id;
        component.AuthorLogin = author.login;
        buttons[buttonIndex].GetComponentInChildren<TextMeshProUGUI>().text = component.AuthorLogin;
    }
}