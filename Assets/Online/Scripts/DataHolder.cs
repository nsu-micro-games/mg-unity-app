using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GamePlayer.Scripts;
using UnityEngine.UI;

public static class DataHolder {

    ////////// back-end address

    public static string BackEndAddress {get;} = "http://82.147.71.147:8080";

    ////////// search author mode

    public enum SearchAuthorMode {
        ADD_FILTER,
        OPEN_PROFILE
    }

    public static SearchAuthorMode CurrentSearchAuthorMode {get; set;}

    ////////// search tag mode

    public enum SearchTagMode {
        ADD_FILTER,
        ADD_DURING_PUBLISHING
    }

    public static SearchTagMode CurrentSearchTagMode {get; set;}

    ////////// my username

    public static string MyUsername {get; set;} = "username";

    ////////// id of chosen user

    public static long IdOfChosenUser {get; set;}

    ////////// currently publishing game title

    public static string CurrentlyPublishingGameTitle {get; set;} = "";

    ////////// my token

    public static string MyToken {get; set;} = "";
    
    ////////// my name

    public static string MyName {get; set;} = "my name";
    
    ////////// my bio

    public static string MyBio {get; set;} = "my bio";
    
    ////////// my avatar

    public static Sprite MyAvatar { get; set; } = (Sprite)Resources.Load("DefaultAvatar", typeof(Sprite));

    ////////// login

    [Serializable]
    public class LoginDto {
        public string login;
        public string password;

        public LoginDto(string login, string password) {
            this.login = login;
            this.password = password;
        }
    }
    
    public class LoginResponse
    {
        public string token;
    }
    
    ////////// registration

    [Serializable]
    public class RegistrationDto {
        public string login;
        public string password;
        public string photoUrl;

        public RegistrationDto(string login, string password, string photoUrl) {
            this.login = login;
            this.password = password;
            this.photoUrl = photoUrl;
        }
    }

    [Serializable]
    public class RegistrationResponse
    {
        public string token;
    }

    ////////// games

    [Serializable]
    public class GameSending {
        public string name;
        public string description;
        public string gameJson;
        public string[] tags;

        public GameSending(string name, string description, string gameJson, string[] tags) {
            this.name = name;
            this.description = description;
            this.gameJson = gameJson;
            this.tags = tags;
        }
    }

    [Serializable]
    public class GameReceived {
        public long id;
        public long authorId;
        public string name;
        public string description;
        public string gameJson;
        public string[] tags;
        public long[] usersLikedIds;
        public long[] usersDislikedIds;
        public long[] usersReportedIds;

        public GameReceived(
            long id, long authorId, string name, string description, string gameJson, string[] tags,
            long[] usersLikedIds, long[] usersDislikedIds, long[] usersReportedIds
        ) {
            this.id = id;
            this.authorId = authorId;
            this.name = name;
            this.description = description;
            this.gameJson = gameJson;
            this.tags = tags;
            this.usersLikedIds = usersLikedIds;
            this.usersDislikedIds = usersDislikedIds;
            this.usersReportedIds = usersReportedIds;
        }
    }

    [Serializable]
    public class Games {
        public GameReceived[] games;

        public Games(GameReceived[] games) {
            this.games = games;
        }
    }

    public static Games CurrentGamesPack {get; set;}

    ////////// games filter

    [Serializable]
    public class GamesFilter {
        public string[] tags;
        public long[] authorIds;
        public int pageSize;
        public int pageNumber;

        public GamesFilter(string[] tags, long[] authorIds, int pageSize, int pageNumber) {
            this.tags = tags;
            this.authorIds = authorIds;
            this.pageSize = pageSize;
            this.pageNumber = pageNumber;
        }
    }

    ////////// authors filter

    [Serializable]
    public class AuthorsFilter {
        public string username;
        public int pageSize;
        public int pageNumber;

        public AuthorsFilter(string username, int pageSize, int pageNumber) {
            this.username = username;
            this.pageSize = pageSize;
            this.pageNumber = pageNumber;
        }
    }

    ////////// authors

    [Serializable]
    public class Author {

        public long id;
        public string login;
        public string passwordHash;
        public string photoUrl;

        public Author(long id, string login, string passwordHash, string photoUrl) {
            this.id = id;
            this.login = login;
            this.passwordHash = passwordHash;
            this.photoUrl = photoUrl;
        }

        public override int GetHashCode() {
            return (int)id;
        }

        public override bool Equals(object obj) {
            if (obj == null) return false;
            Author objAsAuthor = obj as Author;
            if (objAsAuthor == null) return false;
            else return Equals(objAsAuthor);
        }

        public bool Equals(Author other) {
            if (other == null) return false;
            return (this.id.Equals(other.id));
        }
    }

    [Serializable]
    public class Authors {
        public Author[] authors;
    }

    public static List<Author> AvailableAuthors {get; set;} = new List<Author>();

    public static bool AvailableAuthorsPresent() {
        return AvailableAuthors.Count != 0;
    }

    public static void AddAvailableAuthors(List<Author> newAuthors) {
        AvailableAuthors.AddRange(newAuthors);
    }

    public static Author FindAuthorById(long id) {
        foreach (Author author in AvailableAuthors) {
            if (author.id == id) {
                return author;
            }
        }
        return null;
    }

    public static List<Author> SelectedAuthors {get;} = new List<Author>();

    public static long[] GetSelectedAuthorsIds() {
        return SelectedAuthors.Select(a => a.id).ToArray();
    }

    public static void SelectAuthor(Author author) {
        SelectedAuthors.Add(author);
        AvailableAuthors.Remove(author);
    }

    public static void UnselectAuthor(Author author) {
        SelectedAuthors.Remove(author);
        AvailableAuthors.Add(author);
    }

    ////////// tags

    public class Tag {

        public long Id {get; set;}
        public string Name {get; set;}

        public Tag(long id, string name) {
            this.Id = id;
            this.Name = name;
        }

        public override int GetHashCode() {
            return (int)Id;
        }

        public override bool Equals(object obj) {
            if (obj == null) return false;
            Tag objAsTag = obj as Tag;
            if (objAsTag == null) return false;
            else return Equals(objAsTag);
        }

        public bool Equals(Tag other) {
            if (other == null) return false;
            return (this.Id.Equals(other.Id));
        }
    }

    public static List<Tag> AvailableTags {get; set;} = new List<Tag>();

    public static bool AvailableTagsPresent() {
        return AvailableTags.Count != 0;
    }

    public static List<Tag> SelectedTags {get;} = new List<Tag>();

    public static string[] GetSelectedTagsNames() {
        return SelectedTags.Select(t => t.Name).ToArray();
    }

    public static void SelectTag(Tag tag) {
        SelectedTags.Add(tag);
        AvailableTags.Remove(tag);
    }

    public static void UnselectTag(Tag tag) {
        SelectedTags.Remove(tag);
        AvailableTags.Add(tag);
    }

    ////////// filter: common

    public static void ResetAuthorsAndTags() {
        AvailableTags.Clear();
        SelectedTags.Clear();
        AvailableAuthors.Clear();
        SelectedAuthors.Clear();
    }

    ////////// volume: music

    public static float MusicVolume {get; set;} = 0.5f;

    ////////// volume: sound

    public static float SoundVolume {get; set;} = 0.5f;

    ////////// publishing: test passed

    public static bool IsTestPassed {get; set;} = false;
}