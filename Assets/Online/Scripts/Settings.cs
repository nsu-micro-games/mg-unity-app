using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Settings : MonoBehaviour {

    private Slider musicSlider;
    private Slider soundSlider;

    public void Start() {
        float currentMusicVolume = DataHolder.MusicVolume;
        float currentSoundVolume = DataHolder.SoundVolume;

        musicSlider = GameObject.Find("MusicSlider").GetComponent<Slider>();
        soundSlider = GameObject.Find("SoundSlider").GetComponent<Slider>();

        musicSlider.value = currentMusicVolume;
        soundSlider.value = currentSoundVolume;
    }

    public void Back() {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    public void Save() {
        DataHolder.MusicVolume = musicSlider.value;
        DataHolder.SoundVolume = soundSlider.value;
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
}