using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using GamePlayer.Scripts;

public class GameButton : MonoBehaviour {

    public long GameId {get; set;}
    public string GameName {get; set;}

    public void OnClick() {
        StartCoroutine(GamesLoader.LoadGameById(GameId));
    }
}