using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class GamesRate {

    public static IEnumerator LikeRequest(long gameId) {
        Debug.Log("Liking game: " + gameId);
        UnityWebRequest uwr = UnityWebRequest.Post(DataHolder.BackEndAddress + "/api/games/like/" + gameId, "");
        uwr.SetRequestHeader("Authorization", "Bearer_" + DataHolder.MyToken);
        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success) {
            Debug.Log(uwr.error);
        } else {
            Debug.Log("Liked successfully");
        }
    }

    public static IEnumerator DislikeRequest(long gameId) {
        Debug.Log("Disliking game: " + gameId);
        UnityWebRequest uwr = UnityWebRequest.Post(DataHolder.BackEndAddress + "/api/games/dislike/" + gameId, "");
        uwr.SetRequestHeader("Authorization", "Bearer_" + DataHolder.MyToken);
        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success) {
            Debug.Log(uwr.error);
        } else {
            Debug.Log("Disliked successfully");
        }
    }

    public static IEnumerator ReportRequest(long gameId, string reportText) {
        Debug.Log("Reporting game: " + gameId + ". Report text: " + reportText);
        UnityWebRequest uwr = UnityWebRequest.Post(DataHolder.BackEndAddress + "/api/games/report/" + gameId, "");
        uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(reportText));
        uwr.SetRequestHeader("Authorization", "Bearer_" + DataHolder.MyToken);
        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success) {
            Debug.Log(uwr.error);
        } else {
            Debug.Log("Reported successfully");
        }
    }
}