using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class GamesScrolling : MonoBehaviour {

    public GameObject gameButtonPrefab;
    public int buttonsOffset = 5;

    private List<GameObject> buttons;
    private int currentPageNumber = 0;
    private int pageSize = 3;

    public void Start() {
        buttons = new List<GameObject>();
    }
    
    public void ShowMore(long authorId) {
        StartCoroutine(LoadNextGamesPack(authorId));
    }

    private IEnumerator LoadNextGamesPack(long authorId) {
        UnityWebRequest uwr = UnityWebRequest.Get(DataHolder.BackEndAddress + "/api/games/list");
        uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(
            JsonUtility.ToJson(new DataHolder.GamesFilter(
                new string[0], new long[] {authorId}, pageSize, currentPageNumber))));
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.SetRequestHeader("Authorization", "Bearer_" + DataHolder.MyToken);
        yield return uwr.SendWebRequest();

        if (uwr.result == UnityWebRequest.Result.ConnectionError) {
            Debug.Log("Error: " + uwr.error);
        } else {
            string requestResult = uwr.downloadHandler.text;
            Debug.Log("Received: " + requestResult);
            DataHolder.Games newGames = JsonUtility.FromJson<DataHolder.Games>(
                "{ \"games\": " + requestResult + "}");
            List<DataHolder.GameReceived> newGamesList = new List<DataHolder.GameReceived>(newGames.games);
            InitianizeNewButtons(newGamesList);
            currentPageNumber++;
        }
    }

    private void InitianizeNewButtons(List<DataHolder.GameReceived> newGames) {
        int buttonsCount = buttons.Count;
        for (int i = buttonsCount; i < buttonsCount + newGames.Count; i++) {
            GameObject newButton = Instantiate(gameButtonPrefab, transform, false);
            buttons.Add(newButton);
            SetValuesForButton(newButton, newGames[i - buttonsCount]);
            if (i == 0) continue;
            buttons[i].transform.localPosition = new Vector2(
                buttons[i].transform.localPosition.x,
                buttons[i-1].transform.localPosition.y - gameButtonPrefab.GetComponent<RectTransform>().sizeDelta.y);
        }
    }

    private void SetValuesForButton(GameObject button, DataHolder.GameReceived game) {
        GameButton component = (GameButton)button.GetComponent("GameButton");
        component.GameId = game.id;
        component.GameName = game.name;
        button.GetComponentInChildren<TextMeshProUGUI>().text = component.GameName;
    }
}