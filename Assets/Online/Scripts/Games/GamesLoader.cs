using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using GamePlayer.Scripts;

public static class GamesLoader {
    
    private static int currentPageNumber = 0;
    private static int pageSize = 3;

    public static IEnumerator LoadGameById(long gameId) {
        UnityWebRequest uwr = UnityWebRequest.Get(DataHolder.BackEndAddress + "/api/games/" + gameId);
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.SetRequestHeader("Authorization", "Bearer_" + DataHolder.MyToken);
        yield return uwr.SendWebRequest();

        if (uwr.result == UnityWebRequest.Result.ConnectionError) {
            Debug.Log("Error: " + uwr.error);
        } else {
            string requestResult = uwr.downloadHandler.text;
            Debug.Log("Received: " + requestResult);
            DataHolder.GameReceived gameData = JsonUtility.FromJson<DataHolder.GameReceived>(requestResult);
            DataHolder.CurrentGamesPack = new DataHolder.Games(new DataHolder.GameReceived[]{gameData});
            LaunchPlayer();
        }
    }

    public static IEnumerator LoadNextGamesPack() {
        UnityWebRequest uwr = UnityWebRequest.Get(DataHolder.BackEndAddress + "/api/games/list");
        uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(
            JsonUtility.ToJson(new DataHolder.GamesFilter(
                DataHolder.GetSelectedTagsNames(), DataHolder.GetSelectedAuthorsIds(), pageSize, currentPageNumber))));
        uwr.SetRequestHeader("Content-Type", "application/json");
        uwr.SetRequestHeader("Authorization", "Bearer_" + DataHolder.MyToken);
        yield return uwr.SendWebRequest();

        if (uwr.result == UnityWebRequest.Result.ConnectionError) {
            Debug.Log("Error: " + uwr.error);
        } else {
            string requestResult = uwr.downloadHandler.text;
            Debug.Log("Received: " + requestResult);

            DataHolder.Games newGames = JsonUtility.FromJson<DataHolder.Games>(
                "{ \"games\": " + requestResult + "}");
            DataHolder.CurrentGamesPack = newGames;
            currentPageNumber++;
            Debug.Log("Received games size: " + DataHolder.CurrentGamesPack.games.Length);
            if (DataHolder.CurrentGamesPack.games.Length == 0) {
                currentPageNumber = 0;
            }
            LaunchPlayer();
        }
    }

    public static void LaunchPlayer() {
        if (GameManager.gamesQueue == null || !PlayerManager.isPlaying) {
            GameManager.Init();
            FillGamesQueue();
            PlayerManager.StartPlayer();
        } else {
            GameManager.ClearGamesQueue();
            FillGamesQueue();
        }
    }

    public static IEnumerator LoadNewGamesIfQueueIsEmpty() {
        if (GameManager.gamesQueue.IsEmpty) {
            yield return LoadNextGamesPack();
        } else {
            yield return null;
        }
    }

    private static void FillGamesQueue() {
        foreach (DataHolder.GameReceived game in DataHolder.CurrentGamesPack.games) {
            GameManager.gamesQueue.Enqueue(GameToGameData(game));
        }
    }

    private static GameData GameToGameData(DataHolder.GameReceived game) {
        GameDAO gameJson = JsonUtility.FromJson<GameDAO>(game.gameJson);
        return new GameData(game.id, game.authorId, game.name, game.description, gameJson, game.tags,
            game.usersLikedIds, game.usersDislikedIds, game.usersReportedIds);
    }
}