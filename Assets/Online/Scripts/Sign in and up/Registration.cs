using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using TMPro;

public class Registration : MonoBehaviour {

    public void SignUp() {
        string login = GameObject.Find("UsernameIF").GetComponent<TMP_InputField>().text;
        string password = GameObject.Find("PasswordIF").GetComponent<TMP_InputField>().text;
        StartCoroutine(Register(login, password, ""));
    }

    public void Back() {
        SceneManager.LoadScene("Authorization", LoadSceneMode.Single);
    }

    private IEnumerator Register(string login, string password, string photoUrl) {
        UnityWebRequest uwr = UnityWebRequest.Post(DataHolder.BackEndAddress + "/api/profiles/register", "");
        uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(
            JsonUtility.ToJson(new DataHolder.RegistrationDto(
                login, password, photoUrl))));
        uwr.SetRequestHeader("Content-Type", "application/json");
        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success) {
            Debug.Log(uwr.error);
        } else {
            DataHolder.MyToken = uwr.downloadHandler.text;
            Debug.Log("Registered successfully, token = " + DataHolder.MyToken);
            SceneManager.LoadScene("Authorization", LoadSceneMode.Single);
        }
    }
}