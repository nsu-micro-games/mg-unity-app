using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using TMPro;

public class Authorization : MonoBehaviour {

    private TMP_InputField usernameIF;
    private TMP_InputField passwordIF;

    public void Start() {
        usernameIF = GameObject.Find("UsernameIF").GetComponent<TMP_InputField>();
        passwordIF = GameObject.Find("PasswordIF").GetComponent<TMP_InputField>();
    }

    public void SignIn() {
        StartCoroutine(Login(usernameIF.text, passwordIF.text));
    }

    public void SignUp() {
        SceneManager.LoadScene("Registration", LoadSceneMode.Single);
    }

    private IEnumerator Login(string login, string password) {
        UnityWebRequest uwr = UnityWebRequest.Post(DataHolder.BackEndAddress + "/api/profiles/login", "");
        uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(
            JsonUtility.ToJson(new DataHolder.LoginDto(login, password))));
        uwr.SetRequestHeader("Content-Type", "application/json");
        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success) {
            Debug.Log(uwr.error);
        } else {
            DataHolder.MyUsername = login;
            DataHolder.MyToken = uwr.downloadHandler.text;
            Debug.Log("Authorized successfully, token = " + DataHolder.MyToken);
            SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
        }
    }
}