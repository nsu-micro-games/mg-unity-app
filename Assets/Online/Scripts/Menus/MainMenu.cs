using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour {

    public void Start() {
        TextMeshProUGUI welcomeText = GameObject.Find("WelcomeText").GetComponent<TextMeshProUGUI>();
        welcomeText.text = "Welcome, " + DataHolder.MyUsername + "!";
        DataHolder.ResetAuthorsAndTags();
    }

    public void Play() {
        SceneManager.LoadScene("PlayMenu", LoadSceneMode.Single);
    }

    public void Create() {
        DataHolder.IsTestPassed = false;
        DataHolder.CurrentSearchTagMode = DataHolder.SearchTagMode.ADD_DURING_PUBLISHING;
        SceneManager.LoadScene("GameEditorLoad", LoadSceneMode.Single);
    }

    public void Settings() {
        SceneManager.LoadScene("Settings", LoadSceneMode.Single);
    }

    public void LogOut() {
        SceneManager.LoadScene("Authorization", LoadSceneMode.Single);
    }
}