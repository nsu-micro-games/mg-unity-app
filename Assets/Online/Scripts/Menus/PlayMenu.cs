using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayMenu : MonoBehaviour {

    public void Start() {
        DataHolder.ResetAuthorsAndTags();
    }

    public void Play() {
        StartCoroutine(GamesLoader.LoadNextGamesPack());
    }

    public void Filter() {
        DataHolder.CurrentSearchAuthorMode = DataHolder.SearchAuthorMode.ADD_FILTER;
        DataHolder.CurrentSearchTagMode = DataHolder.SearchTagMode.ADD_FILTER;
        SceneManager.LoadScene("Filter", LoadSceneMode.Single);
    }

    public void Search() {
        DataHolder.CurrentSearchAuthorMode = DataHolder.SearchAuthorMode.OPEN_PROFILE;
        SceneManager.LoadScene("SearchAuthor", LoadSceneMode.Single);
    }

    public void Back() {
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }
}