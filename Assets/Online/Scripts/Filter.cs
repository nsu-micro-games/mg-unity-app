using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Filter : MonoBehaviour {

    public void Start() {
        Debug.Log("Authors:");
        foreach(DataHolder.Author author in DataHolder.SelectedAuthors) {
            Debug.Log(author.id);
        }
        Debug.Log("Tags:");
        foreach(DataHolder.Tag tag in DataHolder.SelectedTags) {
            Debug.Log(tag.Id);
        }
    }

    public void Play() {
        StartCoroutine(GamesLoader.LoadNextGamesPack());
    }

    public void PlusAuthor() {
        SceneManager.LoadScene("SearchAuthor", LoadSceneMode.Single);
    }

    public void PlusTag() {
        SceneManager.LoadScene("SearchTag", LoadSceneMode.Single);
    }

    public void Back() {
        SceneManager.LoadScene("PlayMenu", LoadSceneMode.Single);
    }
}