using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using TMPro;

public class UserProfile : MonoBehaviour {

    public GameObject gamesScrolling;

    private long userId;

    public void Start() {
        DataHolder.Author user = DataHolder.FindAuthorById(DataHolder.IdOfChosenUser);

        userId = user.id;

        TextMeshProUGUI userNameText = GameObject.Find("UserNameText").GetComponent<TextMeshProUGUI>();
        userNameText.text = user.login;

        Image userAvatarImage = GameObject.Find("UserAvatar").GetComponent<Image>();
        if (user.photoUrl != null) {
            StartCoroutine(SetAvatarFromUrl(user.photoUrl, userAvatarImage));
        }

        ShowMoreGames();
    }

    public void ShowMoreGames() {
        gamesScrolling.SendMessage("ShowMore", userId);
    }

    public void Back() {
        DataHolder.ResetAuthorsAndTags();
        SceneManager.LoadScene("SearchAuthor", LoadSceneMode.Single);
    }

    private IEnumerator SetAvatarFromUrl(string url, Image avatar) {
        UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url);
        yield return uwr.SendWebRequest();

        if (uwr.result == UnityWebRequest.Result.ConnectionError) {
            Debug.Log(uwr.error);
            yield break;
        }
    
        var texture = DownloadHandlerTexture.GetContent(uwr);;
        var sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), Vector2.one * 0.5f);
        avatar.sprite = sprite;
    }
}