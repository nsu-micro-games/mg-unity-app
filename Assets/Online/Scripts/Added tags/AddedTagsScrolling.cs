using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddedTagsScrolling : MonoBehaviour {

    public GameObject addedTagButtonPrefab;
    public int buttonsOffset = 5;

    private GameObject[] buttons;

    public void Start() {
        List<DataHolder.Tag> tags = DataHolder.SelectedTags;
        buttons = new GameObject[tags.Count];

        for (int i = 0; i < tags.Count; i++) {
            buttons[i] = Instantiate(addedTagButtonPrefab, transform, false);
            SetValuesForButton(i, tags[i]);
            if (i == 0) continue;
            buttons[i].transform.localPosition = new Vector2(
                buttons[i-1].transform.localPosition.x + addedTagButtonPrefab.GetComponent<RectTransform>().sizeDelta.x,
                buttons[i].transform.localPosition.y);
        }
    }

    private void SetValuesForButton(int buttonIndex, DataHolder.Tag tag) {
        AddedTagButton component = (AddedTagButton)buttons[buttonIndex].GetComponent("AddedTagButton");
        component.TagId = tag.Id;
        component.TagName = tag.Name;
        buttons[buttonIndex].GetComponentInChildren<TextMeshProUGUI>().text = component.TagName;
    }
}