using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddedTagButton : MonoBehaviour {

    public long TagId {get; set;}
    public string TagName {get; set;}

    public void OnClick() {
        Destroy(gameObject);
        DataHolder.UnselectTag(new DataHolder.Tag(TagId, TagName));
    }
}