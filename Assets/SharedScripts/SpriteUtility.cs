using System;
using System.IO;
using UnityEngine;
namespace SharedScripts
{
    public static class SpriteUtility
    {
        public static Sprite LoadFromImage(string path)
        {
            var texture = new Texture2D(1, 1);
            if (false == File.Exists(path))
            {
                Debug.LogError($"{nameof(SpriteUtility)}.{nameof(LoadFromImage)} File \"{path}\" does not exist");
                return null;
            }
            
            texture.LoadImage(File.ReadAllBytes(path));
            return Sprite.Create(
                texture,
                new Rect(0, 0, texture.width, texture.height),
                new Vector2(0.5f, 0.5f),
                Math.Max(texture.width, texture.height) / 1.5f
            );
        }
    }
}