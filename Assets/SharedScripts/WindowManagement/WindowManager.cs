using System.Collections.Generic;
using System.Linq;
// using Lean.Touch;
// using UI.Windows.Header;
using UnityEngine;
using UnityEngine.UI;

public class WindowManager : MonoBehaviour
{
    public List<BaseWindow> windows;
    
    public List<BaseWindow> shownWindows;
    private Dictionary<string, BaseWindow> _windowsByName = new Dictionary<string, BaseWindow>();
    
    private void Awake()
    {
        // SC.Instance.Set(this); A.K.: doesn't work on Mac
        foreach (var window in windows)
        {
            _windowsByName.Add(window.windowName, window);
        }
    }
    
    public BaseWindow GetWindow(string windowName) => _windowsByName[windowName];

    public T GetWindow<T>(string windowName) where T : BaseWindow
    {
        var window = GetWindow(windowName);

        if (window is T result)
        {
            return result;
        }

        return default;
    }
    
    public void Show(string windowName)
    {
        Debug.Log($"Try to show {windowName}");
        // Show(_windowsByName[windowName], overlay); A.K.: maybe we need here just Show(_windowsByName[windowName])
    }

    public void Hide(string windowName)
    {
        Debug.Log($"Try to hide {windowName}");
        Hide(_windowsByName[windowName]);
    }

    public void Show(BaseWindow window)
    {
        if (!window)
        {
            Debug.LogError("Window to show is null");
            return;
        }

        int wndIndex = shownWindows.IndexOf(window);
        if (wndIndex != -1)
        {
            shownWindows.RemoveAt(wndIndex);
        }
        shownWindows.Add(window);
        
        Debug.Log($"Show window: {window.windowName}");
        window.OnShow();
    }

    public void Hide(BaseWindow window)
    {
        if (!window)
        {
            Debug.LogError("Window to hide is null");
            return;
        }
   
        shownWindows.Remove(window);
				if (shownWindows.Any())
				{
						Show(shownWindows.Last());
				}
        
        Debug.Log($"Hide window: {window.windowName}");
        window.OnHide();
    }

    public void BackToPrevWindow()
    {
        if (shownWindows.Count < 2)
        {
            Debug.LogError("Only one window in history");
            return;
        }

        Hide(shownWindows.Last()); 
    }

    public void ShowNextWindow(BaseWindow nextWindow)
    {
        var currentWindow = shownWindows.Last();
        Show(nextWindow);
        currentWindow.OnHide();
    }
}
