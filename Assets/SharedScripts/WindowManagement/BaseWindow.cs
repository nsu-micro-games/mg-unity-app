using TMPro;
using UnityEngine;

public abstract class BaseWindow : MonoBehaviour
{
    [Header("Base Window fields")]
    public string windowName;
    public string windowGroupName = "";
    public  bool isShown;
    
    public virtual void OnShow()
    {
        gameObject.SetActive(true);
        isShown = true;
    }

    public virtual void OnHide()
    {
        isShown = false;
        gameObject.SetActive(false);
    }
}
