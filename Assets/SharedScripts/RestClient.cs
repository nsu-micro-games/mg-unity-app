using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class RestClient : MonoBehaviour
{
    public void GetRequest<T>(string uri, Action<bool, string, T> callback, string token = null)
    {
        void Callback(bool success, string result)
        {
            T t = default;
            if (success)
            {
                t = JsonConvert.DeserializeObject<T>(result);
            }

            callback?.Invoke(success, result, t);
        }

        GetRequest(uri, Callback, token);
    }

    public void GetRequest(string uri, Action<bool, string> callback, string token = null)
    {
        StartCoroutine(Get(uri, callback, token));
    }

    private static IEnumerator Get(string uri, Action<bool, string> callback, string token)
    {
        string result;
        var success = true;

        using (var www = UnityWebRequest.Get(uri))
        {
            if (token != null)
            {
                www.SetRequestHeader("Authorization", "Bearer " + token);
            }

            string requestId = Guid.NewGuid().ToString().Replace("-", "");
            www.SetRequestHeader("request-id", requestId);

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                result = www.error;
                success = false;

                Debug.LogError($"GET error: URL:{www.url}\n Id:{requestId}\n Info: {www.error}");
            }
            else
            {
                result = www.downloadHandler.text;
            }
        }

        callback?.Invoke(success, result);
    }

    public void PostRequest<T>(string uri, Dictionary<string, object> fields,
        Action<bool, string, T> callback, string token = null)
    {
        void Callback(bool success, string result)
        {
            T t = default;
            if (success)
            {
                t = JsonConvert.DeserializeObject<T>(result);
            }

            callback?.Invoke(success, result, t);
        }

        PostRequest(uri, fields, Callback, token);
    }

    public void PostRequest<T>(string uri, object fields,
        Action<bool, string, T> callback, string token = null)
    {
        void Callback(bool success, string result)
        {
            T t = default;
            Debug.Log("Post response:\n" + result);
            if (success)
            {
                t = JsonConvert.DeserializeObject<T>(result);
            }

            callback?.Invoke(success, result, t);
        }

        PostRequest(uri, fields, Callback, token);
    }

    public void PostRequest(string uri, Dictionary<string, object> fields,
        Action<bool, string> callback, string token = null)
    {
        StartCoroutine(Post(uri, fields, callback, token));
    }

    public void PostRequest(string uri, object fields,
        Action<bool, string> callback, string token = null)
    {
        StartCoroutine(Post(uri, fields, callback, token));
    }

    private static IEnumerator Post(string uri, object fields,
        Action<bool, string> callback, string token)
    {
        string result;
        var success = true;

        using (var www = UnityWebRequest.Post(uri, ""))
        {
            www.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(
                JsonUtility.ToJson(fields)));
            if (token != null)
            {
                www.SetRequestHeader("Authorization", "Bearer " + token);
            }

            var requestId = Guid.NewGuid().ToString().Replace("-", "");
            www.SetRequestHeader("request-id", requestId);
            www.SetRequestHeader("Accept", "application/json");
            www.SetRequestHeader("Content-Type", "application/json");

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                result = www.error;
                success = false;
                Debug.LogError($"POST error: URL:{www.url}\n Id: {requestId}\n Info: {www.error}");
            }
            else
            {
                result = www.downloadHandler.text;
            }
        }

        callback?.Invoke(success, result);
    }

    private static IEnumerator Post(string uri, Dictionary<string, object> fields,
        Action<bool, string> callback, string token)
    {
        string result;
        var success = true;

        var form = new WWWForm();
        foreach (var pair in fields)
        {
            string value;
            if (pair.Value is string v)
            {
                value = v;
            }
            else
            {
                value = JsonConvert.SerializeObject(pair.Value);
            }

            form.AddField(pair.Key, value);
        }

        using (var www = UnityWebRequest.Post(uri, form))
        {
            if (token != null)
            {
                www.SetRequestHeader("Authorization", "Bearer " + token);
            }

            string requestId = Guid.NewGuid().ToString().Replace("-", "");
            www.SetRequestHeader("request-id", requestId);
            www.SetRequestHeader("Accept", "application/json");
            www.SetRequestHeader("Content-Type", "application/json");

            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                result = www.error;
                success = false;
                Debug.LogError($"POST error: URL:{www.url}\n Id: {requestId}\n Info: {www.error}");
            }
            else
            {
                result = www.downloadHandler.text;
            }
        }

        callback?.Invoke(success, result);
    }
}